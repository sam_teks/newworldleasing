#!/usr/bin/env python
#  -*- coding:utf-8 -*-

# REQUEST INFORMATION
#REQUEST_URL = url = "http://192.168.0.193:5000/classify_request"
REQUEST_URL = url = "http://144.217.4.67:5000/classify_request"
#REQUEST_URL = url = "http://localhost:5000/classify_request"
REQUEST_UID = "NAO"

# FOR NAO SELF CONFIGURE
#NAO_ALPROXY_IP   = "192.168.1.101"
NAO_ALPROXY_IP   = "127.0.0.1"
#NAO_ALPROXY_IP   = "192.168.1.139"
#NAO_ALPROXY_IP   = "192.168.1.139"
NAO_ALPROXY_PORT = 9559

# FOR IFLY LOGIN PARAM
#IFLY_LOGIN_PARAM = "appid = 561db47b, work_dir = ."    # Stanley's APPID
#IFLY_LOGIN_PARAM = "appid = 577dafdf, work_dir = ."    # Densen APPID
IFLY_LOGIN_PARAM = "appid = 57cd222d, work_dir = ."    # Jron's APPID

# FOR NAO TTS OUTPUT WAVE PATH
NAO_TTS_OUTPUT_WAVE_PATH = "/home/nao/teks_nao/softbot/chatbot_flask/out.wav"  # Should be change to your own path

################################################################################

# FOR IFLY PARAM
IFLY_TTS_LINK_LIB_PATH = "./libtts32.so"
IFLY_TTS_LANG_PARAM_CHINESE = "voice_name=xiaoyan,text_encoding=UTF8,sample_rate=16000,speed=50,volume=50,pitch=50,rdn=2"    # Putonghua
# TTS_LANG_PARAM_PUTONGHUA = "voice_name = xiaoyu, text_encoding = UTF8, sample_rate = 16000, speed = 50, volume = 50, pitch = 50, rdn = 2"    # Putonghua
IFLY_TTS_LANG_PARAM_CANTONESE = "voice_name=xiaomei,text_encoding=UTF8,sample_rate=16000,speed=50,volume=100,pitch=45,rdn=2"    # Cantonese
# TTS_LANG_PARAM_CANTONESE = "voice_name = dalong, text_encoding = UTF8, sample_rate = 16000, speed = 50, volume = 50, pitch = 50, rdn = 2"    # Cantonese
IFLY_TTS_LANG_PARAM_ENGLISH   = "voice_name=henry,text_encoding=UTF8,sample_rate=16000,speed=50,volume=80,pitch=50,rdn=2"    # English

# FOR IFLY YUYI
IFLY_IAT_PARAM = "rst=json,sub=iat,ssm=1,auf=audio/L16;rate=16000,sch=1,aue=raw,ent=sms16k,nlp_version=2.0,result_type=json,result_encoding=utf8,language=zh_cn"

# FOR IFLY ASR
MSP_SESSION_PARAM = "rst=plain,ptt=0,nunum=1,sub=iat,ssm=1,auf=audio/L16;rate=16000,aue=raw;7,ent=sms16k,result_type=plain,result_encoding=utf8,vad_speech_tail=1000,language="

# FOR TEXT PROCESS
TEXT_ENGINE_SUCCESS     =  0
TEXT_ENGINE_FAILED      = -1
TEXT_ENGINE_GO_TO_YUYI  = -2
TEXT_ENGINE_EXIT        = -3
TEXT_ENGINE_TRANSLATE   = -4
TEXT_ENGINE_GO_TO_DANCE = -5

LANG_EN = "English"
LANG_CT = "Cantonese"
LANG_CN = "Chinese"

# FOR BRAIN MODIFIED
BRAIN_PATH_MODIFIED   =  2
BRAIN_PATH_INCREASE   =  1
BRAIN_PATH_INVARIABLE =  0
BRAIN_PATH_DECREASE   = -1

# FOR NAO LED COLOR
#NAO_LED_COLOR_ASR_PROCESS = [255, 27, 255]
NAO_LED_COLOR_ASR_PROCESS = [0, 0, 255]
#NAO_LED_COLOR_TTS_PROCESS = [27, 255, 255]
NAO_LED_COLOR_PROCESS_PROCESS = [0, 255, 0]
NAO_LED_COLOR_TTS_PROCESS = [255, 255, 255]

                #langs = ["yue-Hant-HK","cmn-Hans-HK","en-US"]
                #speech_langs = {"yue-Hant-HK":CONFIGURE.LANG_CT,"cmn-Hans-HK":CONFIGURE.LANG_CN,"en-US":CONFIGURE.LANG_CT}
                #langs = ["yue-Hant-HK","cmn-Hans-HK"]
                #speech_langs = {"yue-Hant-HK":CONFIGURE.LANG_CT,"cmn-Hans-HK":CONFIGURE.LANG_CN}
                #langs = ["yue-Hant-HK"]
                #speech_langs = {"yue-Hant-HK":CONFIGURE.LANG_CT}

GOOG_LANG_CN = "cmn-Hans-HK"
GOOG_LANG_CT = "yue-Hant-HK"
GOOG_LANG_EN = "en-US"

SPEECH_GOOGLE_LANGS = [GOOG_LANG_CT]
SPEECH_RECOG_LANGS = {GOOG_LANG_CT:LANG_CT}


BAIDU_LANG_CN = "zh"
BAIDU_LANG_CT = "ct"
BAIDU_LANG_EN = "en"

SPEECH_BAIDU_LANGS = [BAIDU_LANG_CN]
SPEECH_BAIDU_RECOG_LANGS = {BAIDU_LANG_CN:LANG_CN}

FACE_RECOG_FRAMES = {1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,10:10}
VIDEO_FPS = 15
VIDEO_IMAGE_PATH = "/home/nao/teks_images/"
#VIDEO_IMAGE_PATH = "video_images/"
VIDEO_IMAGE_NAME = "camImage_"
VIDEO_FACE_GROUP = "TekHK"
FACE_THRESHOLD = 0.8
FACE_THRESHOLD_2 = 0.7
FACE_TRIES = 10

# FOR RIVESCRIPT BRAIN ROOT PATH
RIVESCRIPT_BRAIN_ROOT_PATH = "./pepper/"
