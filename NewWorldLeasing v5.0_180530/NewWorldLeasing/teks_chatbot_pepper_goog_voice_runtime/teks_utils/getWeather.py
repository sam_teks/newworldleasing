# -*- coding: utf-8 -*-
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import requests
import urllib
import datetime
import re
import getUtils

def getWeather():
    url = "http://rss.weather.gov.hk/rss/CurrentWeather_uc.xml"
    raw = urllib.urlopen(url)
    raw = str(raw.read())

    startpos = raw.find('<author>hkowm@hko.gov.hk</author>')
    endpos = raw.find('<p></p>') + 7 
    raw = raw[startpos:endpos]

    raw = re.sub('/[ ]{2,}|[\t]/', ' ',raw)
    raw = re.sub('/[ ]{1,}|[\t]/', ' ',raw)
    raw = raw.replace(' ', '')
    raw = raw.replace('  ', '')
    raw = raw.replace('<br/>', '')
    raw = raw.replace('\n', '')
    raw = raw.replace('\r', '')
    raw = raw.replace('<p>', '')
    raw = raw.replace('</p>', '')
    raw = raw.replace('</p', '')
    raw = raw.replace('\r\n', '')
    raw = raw.replace('&nbsp;', '')
    raw = re.sub('/\r|\n/', '', raw)
    raw = re.sub('[\s+]', '', raw)
    raw = raw.replace('度相對濕度', '度相對濕度');
    raw = raw.replace('過去', '。過去');
    raw = raw.replace('紫外線強度', '，紫外線強度');

    icon_startpos = raw.find('imgsrc="http://rss.weather.gov.hk/img/') + 38
    icon_endpos = raw.find('.png')
    icon = raw[icon_startpos:icon_endpos]

    showt_startpos = raw.find('氣溫：') + 9
    showt_endpos = raw.find('度相對濕度')
    showt = raw[showt_startpos:showt_endpos]

    showh_startpos = raw.find('百分之') + 9
    showh_endpos = raw.find('。')
    if showh_endpos > 0:
        showh = raw[showh_startpos:showh_startpos+2]
    else:
        showh_endpos = raw.find('<font')
        if showh_endpos < 0:
            showh = raw[showh_startpos:showh_startpos+2]
        else:
            showh = raw[showh_startpos:showh_endpos]
        #showh = raw[showh_startpos:showh_endpos]
    #showh = raw[showh_startpos:]

    jsondata = '{"Errorcode":0,"temperature":' + str(showt) + ',"humidity":' + str(showh) + ',"iconurl":"http://www.hko.gov.hk/content_elements_v2/images/weather-icon-120x120/' + str(icon) + '.png"}'

    return jsondata

def main():
    cd = getWeather()
    print cd

if __name__ == "__main__":
    main()
