# -*- coding: utf-8 -*-
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import requests
import urllib
import datetime
import re

def getNextDayWeather():
	url = "http://rss.weather.gov.hk/rss/SeveralDaysWeatherForecast_uc.xml"
	raw = urllib.urlopen(url)
	raw = str(raw.read())

	startpos = raw.find('<description><![CDATA[')
	endpos = raw.find(']]></description>')
	raw = raw[startpos:endpos]

	raw = re.sub('/[ ]{2,}|[\t]/', ' ',raw)
	raw = re.sub('/[ ]{1,}|[\t]/', ' ',raw)
	raw = raw.replace(' ', '')
	raw = raw.replace('  ', '')
	raw = raw.replace('<br/>', '')
	raw = raw.replace('\n', '')
	raw = raw.replace('\r', '')
	raw = raw.replace('\r\n', '')
	raw = raw.replace('&nbsp;', '')
	raw = raw.replace('至', '-')
	raw = re.sub('/\r|\n/', '', raw)
	raw = re.sub('[\s+]', '', raw)

	datafpos = raw.find('<p/><p/>') + 8
	nextraw = raw[datafpos:]
	endfpos = nextraw.find('<p/><p/>')
	data = nextraw[:endfpos]

	showt_startpos = data.find('氣溫：') + 9
	showt_endpos = data.find('度。相對濕度：')
	showt = data[showt_startpos:showt_endpos]

	showh_startpos = data.find('百分之') + 9
	showh_endpos = showh_startpos + 10
	showh = data[showh_startpos:showh_endpos]
	showh = showh[:-3] 

	url1 = "http://www.weather.gov.hk/wxinfo/currwx/fndc.htm"
	raw1 = urllib.urlopen(url1)
	raw1 = str(raw1.read())

	startipos = raw1.find('/images/wxicon/') + 15
	endipos = startipos + 9
	icon = raw1[startipos:endipos]
	
	jsondata = '{"Errorcode":0,"temperature":"' + str(showt) + '","humidity":"' + str(showh) + '","iconurl":"http://www.hko.gov.hk/content_elements_v2/images/weather-icon-120x120/' + str(icon) + '"}'

	return jsondata

def main():
	cd = getNextDayWeather()
	print cd

if __name__ == "__main__":
	main()
