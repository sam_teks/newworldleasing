# -*- coding: utf8 -*-

import time
import random
import re

import itertools

def match_concept(text, conceptWords):
    return any(word in text for word in conceptWords)

def exactmatch_concept(text, conceptWords):
    return any(word == text for word in conceptWords)

def rand_answer(conceptAnswers):
	random.seed(time.time())
	return random.choice(conceptAnswers)
    
def find_delimiter(text):
    # for baidu ASR
    splitChar = ","
    if text.find(u"？") >= 0:
        splitChar = "？"
    elif text.find(u"，") >= 0:
        splitChar = "，"
    return splitChar

def enDigit2Digit(nd, simp=True, o=False):
	"""
	Converts english to numbers representations.

	`simp`  : use simplified characters instead of traditional characters.
	`o`	 : use 〇 for zero.
	"""

	# initiate variables
	#c_basic = u"〇一二三四五六七八九" if o else u"零一二三四五六七八九"
	#d_basic = "0123456789"
	e_basic = {"zero":"0","one":"1","two":"2","three":"3","four":"4","five":"5","six":"6","seven":"7","eight":"8","nine":"9"}
	
	listChars = nd.split(" ")
	result = []
	numResult = []
	foundNum = False
	for w in listChars:
		digit = e_basic.get(w.lower(),None)
		if digit:
			foundNum = True
			numResult.append(digit)				
		else:
			if foundNum:
				result.append("".join(numResult))
				foundNum = False
				numResult = []
			result.append(w)
	if foundNum:
		result.append("".join(numResult))
	
	return " ".join(result)

def ctDigit2Digit(nd, simp=True, o=False):
	"""
	Converts english to numbers representations.

	`simp`  : use simplified characters instead of traditional characters.
	`o`	 : use 〇 for zero.
	"""

	# initiate variables
	c_basic = {u"零":"0",u"一":"1",u"二":"2",u"三":"3",u"四":"4",u"五":"5",u"六":"6",u"七":"7",u"八":"8",u"九":"9"}
	cn_basic = {"0":"0","1":"1","2":"2","3":"3","4":"4","5":"5","6":"6","7":"7","8":"8","9":"9"}
	
	result = []
	numResult = []
	wordResult = []
	foundNum = False
	nd = nd.decode("utf8").replace(u"七十一",u"七一")
	nd = nd.replace(u"七仔",u"7仔")
	for i in range(len(nd)):
		digit = c_basic.get(nd[i],None)
		if not digit:
			digit = cn_basic.get(nd[i],None)
		if digit:
			foundNum = True
			numResult.append(digit)				
		else:
			if foundNum:
				if wordResult:
					result.append("".join(wordResult))
					wordResult = []
				result.append("".join(numResult))
				foundNum = False
				numResult = []
			wordResult.append(nd[i])

	if foundNum and wordResult:
		result.append("".join(wordResult))
		result.append("".join(numResult))
	elif foundNum:
		result.append("".join(numResult))
	elif wordResult:
		result.append("".join(wordResult))
	
	return " ".join(result)

def cnDigit2Digit(nd, simp=True, o=False):
	"""
	Converts english to numbers representations.

	`simp`  : use simplified characters instead of traditional characters.
	`o`	 : use 〇 for zero.
	"""

	# initiate variables
	c_basic = {"0":"0","1":"1","2":"2","3":"3","4":"4","5":"5","6":"6","7":"7","8":"8","9":"9"}
	
	result = []
	numResult = []
	wordResult = []
	foundNum = False
	for i in range(len(nd)):
		digit = c_basic.get(nd[i],None)
		if digit:
			foundNum = True
			numResult.append(digit)				
		else:
			if foundNum:
				if wordResult:
					result.append("".join(wordResult))
					wordResult = []
				result.append("".join(numResult))
				foundNum = False
				numResult = []
			wordResult.append(nd[i])
			
	if foundNum and wordResult:
		result.append("".join(wordResult))
		result.append("".join(numResult))
	elif foundNum:
		result.append("".join(numResult))
	elif wordResult:
		result.append("".join(wordResult))
	
	return " ".join(result)

def extractCNDigits(nd):
	# initiate variables
	c_basic = {"0":"0","1":"1","2":"2","3":"3","4":"4","5":"5","6":"6","7":"7","8":"8","9":"9"}
	
	result = []
	numResult = []
	wordResult = []
	foundNum = False
	for i in range(len(nd)):
		digit = c_basic.get(nd[i],None)
		if digit:
			foundNum = True
			numResult.append(digit)				
		else:
			if foundNum:
				if wordResult:
					result.append("".join(wordResult))
					wordResult = []
				result.append("".join(numResult))
				foundNum = False
				numResult = []
			wordResult.append(nd[i])
			
	if foundNum and wordResult:
		result.append("".join(wordResult))
		result.append("".join(numResult))
	elif foundNum:
		result.append("".join(numResult))
	elif wordResult:
		result.append("".join(wordResult))
	
	return " ".join(result)

def hasNumber(text, lang=None):
    first_text = text

    if (lang=="en"):
        first_text = enDigit2Digit(text,True,False)
    elif (lang=="ct"):
        first_text = ctDigit2Digit(text,True,False)
        first_text = first_text.encode("utf8")
    elif (lang=="cn"):
        first_text = cnDigit2Digit(text,True,False)

    return re.findall(r'\d+', first_text)    

def isNumber(text):
    first_text = text

    return re.findall(r'^\d+$', first_text)    

def convertWithNumber(text, lang=None):
    first_text = text

    if (lang=="en"):
        first_text = enDigit2Digit(first_text,True,False)
    elif (lang=="ct"):
        first_text = ctDigit2Digit(first_text,True,False)
        first_text = first_text.encode("utf8")
    elif (lang=="cn"):
        first_text = cnDigit2Digit(first_text,True,False)

    return first_text    

def extractNumbers(text, lang=None):
    first_text = text
    numbers = []

    if (lang=="en"):
        first_text = enDigit2Digit(text,True,False)
    elif (lang=="ct"):
        first_text = ctDigit2Digit(text,True,False)
        first_text = first_text.encode("utf8")
    elif (lang=="cn"):
        first_text = cnDigit2Digit(text,True,False)

    numbers = [n for n in first_text.split(" ") if hasNumber(n, lang) and isNumber(n)]
    
    return numbers

def num2chinese(num, big=False, simp=True, o=False, twoalt=False):
    """
    Converts numbers to Chinese representations.

    `big`   : use financial characters.
    `simp`  : use simplified characters instead of traditional characters.
    `o`     : use 〇 for zero.
    `twoalt`: use 两/兩 for two when appropriate.

    Note that `o` and `twoalt` is ignored when `big` is used, 
    and `twoalt` is ignored when `o` is used for formal representations.
    """
    # check num first
    nd = str(num)
    if abs(float(nd)) >= 1e48:
        raise ValueError('number out of range')
    elif 'e' in nd:
        raise ValueError('scientific notation is not supported')
    c_symbol = u'正负点' if simp else u'正負點'
    if o:  # formal
        twoalt = False
    if big:
        c_basic = u'零壹贰叁肆伍陆柒捌玖' if simp else u'零壹貳參肆伍陸柒捌玖'
        c_unit1 = u'拾佰仟'
        c_twoalt = u'贰' if simp else u'貳'
    else:
        c_basic = u'〇一二三四五六七八九' if o else u'零一二三四五六七八九'
        c_unit1 = u'十百千'
        if twoalt:
            c_twoalt = u'两' if simp else u'兩'
        else:
            c_twoalt = u'二'
    c_unit2 = u'万亿兆京垓秭穰沟涧正载' if simp else u'萬億兆京垓秭穰溝澗正載'
    revuniq = lambda l: ''.join(k for k, g in itertools.groupby(reversed(l)))
    nd = str(num)
    result = []
    if nd[0] == '+':
        result.append(c_symbol[0])
    elif nd[0] == '-':
        result.append(c_symbol[1])
    if '.' in nd:
        integer, remainder = nd.lstrip('+-').split('.')
    else:
        integer, remainder = nd.lstrip('+-'), None
    if int(integer):
        splitted = [integer[max(i - 4, 0):i]
                    for i in range(len(integer), 0, -4)]
        intresult = []
        for nu, unit in enumerate(splitted):
            # special cases
            if int(unit) == 0:  # 0000
                intresult.append(c_basic[0])
                continue
            elif nu > 0 and int(unit) == 2:  # 0002
                intresult.append(c_twoalt + c_unit2[nu - 1])
                continue
            ulist = []
            unit = unit.zfill(4)
            for nc, ch in enumerate(reversed(unit)):
                if ch == '0':
                    if ulist:  # ???0
                        ulist.append(c_basic[0])
                elif nc == 0:
                    ulist.append(c_basic[int(ch)])
                elif nc == 1 and ch == '1' and unit[1] == '0':
                    # special case for tens
                    # edit the 'elif' if you don't like
                    # 十四, 三千零十四, 三千三百一十四
                    ulist.append(c_unit1[0])
                elif nc > 1 and ch == '2':
                    ulist.append(c_twoalt + c_unit1[nc - 1])
                else:
                    ulist.append(c_basic[int(ch)] + c_unit1[nc - 1])
            ustr = revuniq(ulist)
            if nu == 0:
                intresult.append(ustr)
            else:
                intresult.append(ustr + c_unit2[nu - 1])
        result.append(revuniq(intresult).strip(c_basic[0]))
    else:
        result.append(c_basic[0])
    if remainder:
        result.append(c_symbol[2])
        result.append(''.join(c_basic[int(ch)] for ch in remainder))
    return ''.join(result)

def strwithnum2chinese(input_text):   
    input_text = input_text.decode("utf8")
    input_text_check = input_text
    output_text = ""
    digit=None
    index=0

    for i in input_text_check:
        index+=1
        if i.isdigit():
            if digit==None:
                digit=i
                #print "Find number at location: "+str(index)
                output_text+=input_text[0:index-1]
                #print "out-"+str(output_text)
                input_text=input_text[index-1:]
                #print "in -"+str(input_text)
                index=1
            else:
                digit+=i
        else:
            if digit!=None:
                #print "Find digit: "+digit
                output_text+=num2chinese(int(digit))
                #print "out-"+str(output_text)
                input_text=input_text[index-1:]
                #print "in -"+str(input_text)
                index=1
                digit=None
                
    if digit==None:
        output_text+=input_text[0:]
    else:
        #print "Find digit: "+digit
        output_text+=num2chinese(int(digit))
        ##input_text=input_text[index-1:]
        ##digit=None
        
    return output_text.encode("utf8")
    
def num2english(nd, simp=True, o=False):
	"""
	Converts english to numbers representations.

	`simp`  : use direct digit to word if True.  if False, convert to whole number to English (NOT DONE YET)
	`o`	 : use 〇 for zero.
	"""

	# initiate variables
	c_basic = {"0":"zero","1":"one","2":"two","3":"three","4":"four","5":"five","6":"six","7":"seven","8":"eight","9":"nine"}
	
	result = []
	numResult = []
	wordResult = []
	foundNum = False
	for i in range(len(nd)):
		digit = c_basic.get(nd[i],None)
		if digit:
			foundNum = True
			numResult.append(digit)				

	return " ".join(numResult)
