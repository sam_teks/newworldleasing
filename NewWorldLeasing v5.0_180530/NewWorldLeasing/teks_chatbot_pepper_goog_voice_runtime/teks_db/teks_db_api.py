#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json

import sqlite3

import teks_db_configure as CONFIGURE

################################################################################

class db_log_module(object):

  def __init__(self,db_file):
    try:
      self.conn = sqlite3.connect(db_file, check_same_thread=False)
      #print("connect db successfully")
    except Exception as e:
      #print("connect db: "+str(e))
      self.conn = None
    pass

  def close(self):
    if self.conn:
      #print("close connection")
      self.conn.close()

  def clear_counter(self,tablename,field):
    if self.conn:
      print("clear all counters")
      sql = "UPDATE "+tablename+" SET "+field+" = 0"
      self.conn.execute(sql)
      self.conn.commit()

  def inc_counter_by_key(self,tablename,key,reply,timestamp):
    if self.conn:
      try:
        sql = "INSERT OR IGNORE INTO "+tablename+" (ITEM_ID,REPLY,TIME) VALUES ('"+key+"','"+reply+"','"+timestamp+"')"
        self.conn.execute(sql)
        #sql = "UPDATE "+tablename+" SET "+field+" = "+field+" + 1 WHERE item_id = '"+key+"'"
        #self.conn.execute(sql)
        self.conn.commit()
      except Exception as e:
        print("update record inc counter: "+str(e))
    pass

  def get_counter_by_key(self,tablename,key,field):
    if self.conn:
      try:
        sql = "SELECT "+field+" FROM "+tablename+" WHERE item_id = '"+key+"'"
        cursor = self.conn.execute(sql)
        for row in cursor:
            for i in range(len(row)):
              print row[i]

      except Exception as e:
        print("get counter record by key: "+key+" "+str(e))
    pass

  def get_counter_for_all(self,tablename,field):
    if self.conn:
      try:
        results = []
        sql = "SELECT item_id,"+field+" FROM "+tablename
        cursor = self.conn.execute(sql)
        for row in cursor:
            results.append({"itemid":row[0],"counter":row[1]})
        return results
      except Exception as e:
        print("get counter record: "+str(e))
        return None
    pass

################################################################################

def main():
  try:
    ui_db = db_log_module(CONFIGURE.UI_DB_FILE)
    '''
    ui_db.inc_counter_by_key("ui_counters","Washroom","counter")
    ui_db.inc_counter_by_key("ui_counters","Washroom","counter")
    ui_db.inc_counter_by_key("ui_counters","btn_find_cc","counter")
    ui_db.inc_counter_by_key("ui_counters","Washroom","counter")
    ui_db.inc_counter_by_key("ui_counters","Washroom","counter")
    ui_db.inc_counter_by_key("ui_counters","btn_find_cc","counter")
    ui_db.get_counter_by_key("ui_counters","Washroom")
    ui_db.get_counter_by_key("ui_counters","btn_find_cc")
    #ui_db.clear_counter("ui_counters","counter")
    '''
    ui_db.get_counter_by_key("ui_counters","Washroom","counter")
    ui_db.get_counter_by_key("ui_counters","btn_find_cc","counter")
    print ui_db.get_counter_for_all("ui_counters","counter")

  except Exception as e:
    print(str(e))
  finally:
    ui_db.close()

################################################################################

if __name__ == "__main__":
  main()
