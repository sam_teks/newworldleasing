
Feature				0-60 minutes		61+ - 1 million minutes
Speech Recognition	Free				$0.006 / 15 seconds*


.BASHRC - where cantoneseproj_1f02e10d2092.json is the key to the GOOGLE Speech Service

export LD_LIBRARY_PATH=/home/nao/.local/lib:$LD_LIBRARY_PATH
export PYTHONPATH=/home/nao/.local/lib/python2.7/site-packages:$PYTHONPATH
export GOOGLE_APPLICATION_CREDENTIALS=/home/nao/chatbot_pepper_4.0_goog_voice/GoogleSpeech/cantoneseproj_1f02e10d2092.json


INSTALL GOOGLE SPEECH API

cd chatbot_pepper_4.0_goog_voice/GoogleSpeech
pip install -r requirements.txt --target /home/nao/.local/lib/python2.7/site-packages

Teks/FoundFace
Teks/NotFoundFace

Teks/FaceDetected
Teks/NotFaceDetected
Teks/ResumeFaceDetect

Teks/StartFaceDetect
Teks/StopFaceDetect

Teks/StopProcessText
Teks/StartProcessText

