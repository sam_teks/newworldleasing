#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import teks_configure as CONFIGURE

eng_stag = "<en>"
eng_stag_len = len(eng_stag)
eng_etag = "</en>"
eng_etag_len = len(eng_etag)

chn_stag = "<cn>"
chn_stag_len = len(eng_stag)
chn_etag = "</cn>"
chn_etag_len = len(eng_etag)


def parse_text(text, lang):
    foundEng = False
    foundChn = False

    speech_list = []

    tmp_text = text
    
    startpos1 = tmp_text.find(eng_stag)
    startpos2 = tmp_text.find(chn_stag)
    if startpos1<0 and startpos2<0:
        speech_list.append((lang,tmp_text))
        startpos = -1
    else:
        if startpos2<0:
            endTag = eng_etag
            startpos = startpos1
        elif startpos1<0:
            endTag = chn_etag
            startpos = startpos2
        elif startpos1<startpos2:
            endTag = eng_etag
            startpos = startpos1
        else:
            endTag = chn_etag
            startpos = startpos2
            
    while startpos >= 0:
        endpos = tmp_text.find(endTag)
        if endpos < 0:
            speech_list.append((lang,tmp_text))
            break

        extracted_str1 = tmp_text[:startpos]
        extracted_str2 = tmp_text[startpos+eng_stag_len:endpos]
        tmp_text = tmp_text[endpos+eng_etag_len:]
        speech_list.append((lang,extracted_str1))
        if endTag==chn_etag:
            speech_list.append((CONFIGURE.LANG_CN,extracted_str2))
        else:
            speech_list.append((CONFIGURE.LANG_EN,extracted_str2))
        '''
        print extracted_str1
        print extracted_str2
        print tmp_text
        '''
        startpos1 = tmp_text.find(eng_stag)
        startpos2 = tmp_text.find(chn_stag)
        if startpos1<0 and startpos2<0:
            speech_list.append((lang,tmp_text))
            startpos = -1
        else:
            if startpos2<0:
                endTag = eng_etag
                startpos = startpos1
            elif startpos1<0:
                endTag = chn_etag
                startpos = startpos2
            elif startpos1<startpos2:
                endTag = eng_etag
                startpos = startpos1
            else:
                endTag = chn_etag
                startpos = startpos2

    return speech_list

def speech_loop(text, lang, tts_proxy, ani_tts_proxy, outside_ct_tts_proxy, outside_en_tts_proxy):
    speech_list = parse_text(text, lang)
    #print speech_list
    for (cur_lang, speech_text) in speech_list:
        #print cur_lang
        #print speech_text
        if cur_lang == CONFIGURE.LANG_CT:
            outside_ct_tts_proxy.say_text(speech_text)
        elif cur_lang == CONFIGURE.LANG_CN:
            tts_proxy.setLanguage("Chinese")
            ani_tts_proxy.say(speech_text)
        elif cur_lang == CONFIGURE.LANG_EN:
            outside_en_tts_proxy.say_text(speech_text)
            ##tts_proxy.setLanguage("English")
            ##ani_tts_proxy.say(speech_text)

    pass

if __name__ == '__main__':
    from naoqi import ALProxy
    from naoqi import ALModule
    from naoqi import ALBroker
    import qi

    ip = "127.0.0.1"
    port = 9559
    myBroker = ALBroker("myBroker",
        "0.0.0.0",    # listen to anyone
        0,            # find a free port and use it
        ip,          # parent broker IP
        port)        # parent broker port

    tts_proxy = ALProxy("ALTextToSpeech", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
    animated_speech_proxy = ALProxy("ALAnimatedSpeech", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
    ct_tts_proxy = ALProxy("cantonese_tts_service", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)

    #speech_loop("你搞到人地<en>Price Rite dot com dot hk</en>唔好意思嘅",CONFIGURE.LANG_CT, tts_proxy, animated_speech_proxy, outside_tts_proxy)
