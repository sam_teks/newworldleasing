#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import sys
import time
import urllib
import json
import requests
import threading
import random

################################################################################
__version__ = '0.0.1'
__copyright__ = 'Copyright 2018, Teksbotics (Hong Kong) Ltd.'
__author__ = 'Alan Yan'
__email__ = 'support@teksbotics.com'
################################################################################

#class processtext_tuling_thread():
def get(input_text):
    #time.sleep(100)
    uid = ""
    url = "http://www.tuling123.com/openapi/api"
    
    try:
        #input_text = sys.argv[1]
        #encode('utf-8').strip()
        #print str(input_text.encode('utf-8').strip())
        
        def getHtml(url):
            page=urllib.urlopen(url)
            html=page.read()
            return html
        
        key = 'eee9c0a2b51444058f2dc58f608b8c28'
        #key = '955bd3cff5fd4562ba0a952e3500574e'
        #api = 'http://www.tuling123.com/openapi/api?key=' + key + '&info='
        #request = api + input_text
        #url = request
        #response = getHtml(request)
        
        #dic_json = json.loads(response)
        #print "AAA "+dic_json['text'].encode("utf8")
      
        #say_text(tts_thread, dic_json['text'].encode("utf8"), CURRENT_LANGUAGE)
        
        input_json = {"key": key, "info": input_text};
        start_time = time.time()
        req = requests.post(url, input_json)
        
        if req:
            print "\n*** REQUEST RETURNS ***, time: "+str(time.time() - start_time)+"\n"
            #print req.content
            json_data = json.loads(req.content)
            
            respond_text=json_data["text"].encode('utf-8')
                #req_content = str(req.content)
                #print "req_content: " + req_content
                #json_data = json.loads(req.text)
            print "reply_text: " + respond_text

            return respond_text

    except Exception as e:
        print e
        pass

###############################################################

if __name__ == "__main__":
    respond_text = get(sys.argv[1])

