
import threading
import logging

import speech_recognition_teks as sr

class MultiRecognition(object):
    def __init__(self):
        self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}
        self.mylock = threading.RLock()

    def multiThreading(self, recogFunc, audioData, lang_list):
        threads = []
        for i in lang_list:
            threads.append(threading.Thread(target=self.recognition, args=(recogFunc, audioData, i)))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        self.result_set = self.result
        self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}

        return self.result_set

    def recognition(self, recogFunc, audioData, lang):

        said_text = ""
        confidence = 0
        try:
            said_text, confidence = recogFunc(audioData, None, lang)
        except sr.UnknownValueError:
            logging.info("Sorry, not understand in multi stt")
            #self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}
            
        except sr.RequestError as e:
            logging.info("No connection")
            #self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}

        self.mylock.acquire()
        if self.result['confidence'] < confidence:
            self.result['confidence'] = confidence
            self.result['transcript'] = said_text
            self.result['lang_code'] = lang
        self.mylock.release()



