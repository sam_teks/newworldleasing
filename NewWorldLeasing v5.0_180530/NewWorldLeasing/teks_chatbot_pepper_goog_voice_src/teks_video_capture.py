#!/usr/bin/python
# -*- coding: utf-8 -*-

############################################################

import sys
import time
import ctypes
import vision_definitions
import Image
import io
import json

import logging

from naoqi import ALProxy
from naoqi import ALBroker
import qi

import teks_configure as CONFIGURE

import threading

import multi_faceid

################################################################################
pill2kill = threading.Event()
rlock = threading.RLock()

from cw_utils import cw_api
def cw_image_identify(idx):
    global rlock

    start_time = time.time() 
    filename = CONFIGURE.VIDEO_IMAGE_PATH+CONFIGURE.VIDEO_IMAGE_NAME+str(idx)+".png"
    json_data = cw_api.image_identify(CONFIGURE.VIDEO_FACE_GROUP, filename)
    elapsed_time = time.time() - start_time
    with rlock:
        '''
        print "thread "+str(idx)+": "+filename
        print "thread "+str(idx)+": "+str(elapsed_time)
        print "thread "+str(idx)+": result from CW: ", json_data
        '''
        if "faces" in json_data and len(json_data["faces"]) > 0:
            #print json_data["faces"][0]
            #print json_data["faces"][0]["score"]
            '''
            if json_data["faces"][0]["score"] >= CONFIGURE.FACE_THRESHOLD:
                print json_data["faces"][0]["faceId"]
            '''
            #print json_data["faces"][0]["faceId"]
            return json_data["faces"][0]["faceId"], json_data["faces"][0]["score"]
    return "", 0

################################################################################
class VideoDevice(object):
    def __init__(self):

        self.vd_proxy = ALProxy("ALVideoDevice", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        self.image = None
        self.subscribeID = None

    def __def__(self):
        pass

    def startVideo(self, element, FPS, color, format):
        element = "video_buffer"
        #FPS = 10; 
        #color = 0; 
        #format = 0;
        ID = element + '_camera';
        self.subscribeID = self.vd_proxy.subscribeCamera(ID, 0, format, color, FPS)
        print("subscribe: "+self.subscribeID)        

    def stopVideo(self):
        if self.subscribeID and self.vd_proxy.unsubscribe(self.subscribeID):
            print("unsubscribe: "+self.subscribeID)
        else:
            print("NOT SUCCESS - unsubscribe")

    def getImage(self):
        image = None
        if self.subscribeID:
            image = self.vd_proxy.getImageRemote(self.subscribeID)
            if image:
                #print(str(image[0])+"x"+str(image[1]))
                if self.vd_proxy.releaseImage(self.subscribeID):
                    #print("release image")
                    pass

        return image

################################################################################

class teks_video_capture_thread(threading.Thread):
    def __init__(self, stop_event, rlock):
        threading.Thread.__init__(self)

        self.thread_stop_event = stop_event
        self.thread_rlock = rlock
        
        self.thread_stop = False
        self.suspend_flag = True
        
        self.text = ""
        self.vd = VideoDevice()
        self.subscribeID = None

        self.stop_thread = False

        self.image = None
        self.result_set = None

        self.recog = multi_faceid.MultiRecognition()
        self.mem_proxy = ALProxy("ALMemory", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)

    def set_input(self, text = ""):
        self.text = text

    def get_output(self):
        return self.text

    def get_result(self):
        return self.result_set

    def stopVideo(self):
        self.vd.stopVideo()

    def stop(self):
        with rlock:
            self.stop_thread = True

    def save_image(self, image, frame_num):
        imageWidth = image[0]
        imageHeight = image[1]
        array = image[6]
        #image_string = str(bytearray(array))
        image_string = str(bytearray(array))

        # Create a PIL Image from our pixel array.
        #im = Image.fromstring("RGB", (imageWidth, imageHeight), image_string)
        #im = Image.frombytes("RGB", (imageWidth, imageHeight), image_string)
        im = Image.frombytes("L", (imageWidth, imageHeight), image_string)
        #im = Image.frombytes("RGB", imageWidth*imageHeight, image_string)

        # Save the image.
        im.save(CONFIGURE.VIDEO_IMAGE_PATH+CONFIGURE.VIDEO_IMAGE_NAME+str(frame_num)+".png", "PNG")
        pass

    def run(self):
        element = "video_buffer"
        FPS = frame_num = CONFIGURE.VIDEO_FPS; 
        color = 0;  #http://doc.aldebaran.com/2-5/family/pepper_technical/video_pep.html#cameracolorspace-ov5640
        format = 1; #http://doc.aldebaran.com/2-5/family/pepper_technical/video_pep.html#cameraresolution-ov5640
        self.vd.startVideo(element, FPS, color, format)
        #self.image = self.vd.getImage()
        threads = []
        print("start collecting images")
        print("# of frames: "+str(frame_num))
        logging.info("start collecting images")
        while frame_num > 0:
            try:
                self.image = self.vd.getImage()
                if self.image:
                    cur_image = self.image
                    threads.append(threading.Thread(target=self.save_image, args=(cur_image, frame_num)))

                    '''
                    imageWidth = self.image[0]
                    imageHeight = self.image[1]
                    array = self.image[6]
                    #image_string = str(bytearray(array))
                    image_string = str(bytearray(array))

                    # Create a PIL Image from our pixel array.
                    #im = Image.fromstring("RGB", (imageWidth, imageHeight), image_string)
                    #im = Image.frombytes("RGB", (imageWidth, imageHeight), image_string)
                    im = Image.frombytes("L", (imageWidth, imageHeight), image_string)
                    #im = Image.frombytes("RGB", imageWidth*imageHeight, image_string)

                    # Save the image.
                    im.save(CONFIGURE.VIDEO_IMAGE_PATH+CONFIGURE.VIDEO_IMAGE_NAME+str(frame_num)+".png", "PNG")
                    '''
                    #im.show()
            except Exception as e:
                #self.vd.stopVideo()
                print(str(e))
                break
            time.sleep(1/FPS)
            frame_num -= 1
        print("end collecting images")
        logging.info("end collecting images")

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        '''
        while not self.stop_thread and not self.thread_stop_event.is_set():
            try:
                image = self.vd.getImage()
            except KeyboardInterrupt:
                #self.vd.stopVideo()
                break
            time.sleep(1/FPS)
        '''
        self.vd.stopVideo()

        self.result_set = self.recog.multiThreading(cw_image_identify, CONFIGURE.VIDEO_FPS)
        if self.result_set and self.result_set["confidence"] > 0:
            print str(self.result_set)
            self.mem_proxy.raiseEvent('Teks/FoundFace', json.dumps(self.result_set))
        else:
            print "NotFoundFace"
            self.mem_proxy.raiseEvent('Teks/NotFoundFace', "1")

################################################################################

if __name__ == "__main__":

    vc_thread = teks_video_capture_thread(pill2kill, rlock)
    vc_thread.start()

    '''
    while True:
        try:
            pass
        except KeyboardInterrupt:
            print("ctrl-c is pressed.")
            break

    vc_thread.stopVideo()
    vc_thread.stop()
    '''

    pill2kill.set()
    vc_thread.join()
