#!/usr/bin/python
# -*- coding: utf-8 -*-

############################################################
import sys
import time
from rivescript_teks import RiveScript

import teks_configure as CONFIGURE

import logging
logging.basicConfig(level = logging.INFO)

CURRENT_LANGUAGE_TO_RS = {CONFIGURE.LANG_CN: "cn", \
                    CONFIGURE.LANG_CT: "ct", \
                    CONFIGURE.LANG_EN: "en"}

class omba_processor(object):
    def __init__(self, brains_path):
        self.bot = RiveScript(utf8=True,debug=False)
        self.bot.load_directory(brains_path)
        #bot.load_directory("./pepper/ct/brain_cc")
        self.bot.sort_replies()
        pass

    def process_text(self, value):
        reply = self.bot.reply(CONFIGURE.REQUEST_UID, value)
        if "ERR" in reply: reply = ""
        return reply

if __name__ == "__main__":
    pass