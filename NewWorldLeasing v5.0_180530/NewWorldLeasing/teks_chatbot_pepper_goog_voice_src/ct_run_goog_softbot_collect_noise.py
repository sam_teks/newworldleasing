# -*- coding: utf8 -*-

import os
import sys
import time
import pyaudio
import numpy as np
import unicodecsv
import uuid

import logging

import speech_recognition_teks as sr
import random

from rivescript_teks import RiveScript
import re

import jieba
import jieba.analyse

#=======================================
jieba.initialize()

'''
#jieba.load_userdict("./extra_dict/dict_hkia.txt.big")
f = open("./extra_dict/jieba_words_softbot.txt", 'rb')
reader = unicodecsv.reader(f, encoding='utf-8')
for rows in reader:
    for word in rows:
        jieba.add_word(word) 
'''
#jieba.analyse.set_stop_words("./extra_dict/stop_words_hkia.txt")
#jieba.analyse.set_idf_path("./extra_dict/idf_hkia.txt.big");
#=======================================

import threading

rlock = threading.RLock()

'''
bot = RiveScript(utf8=True,debug=False)
bot.load_directory("./pepper/ct/brain")
#bot.load_directory("./pepper/ct/brain_cc")
#bot.load_directory("./pepper/brain")
bot.sort_replies()
'''

PEPPER_ON = False

logging.basicConfig(level=logging.INFO)

listeningFlag = False
session = None

behaviorPath = os.path.dirname(os.path.abspath(__file__))
libPath = behaviorPath
if libPath not in sys.path:
    sys.path.insert(0, libPath)
os.environ["LD_LIBRARY_PATH"] = libPath

##########################################################################################
def runMain():
    try:

        global listeningFlag
        global rlock

        n = 0
        c1 =None

        h = open("./logs/collect_nose_"+str(uuid.uuid1())+".txt",'wb')
        writer = unicodecsv.writer(h, encoding='utf-8')

        first_text = None
        listeningFlag = True

        r = sr.Recognizer()
        device_index = 7
        misscount = 0
        while True:
            #r.energy_threshold = 2500
            with sr.Microphone(device_index,16000,2048) as source:
                r.adjust_for_ambient_noise(source)
                logging.info("checked minimum energy threshold to {}".format(r.energy_threshold))

                try:
                    with rlock:
                        writer.writerow([str(time.time()),"checked minimum energy threshold to {}".format(r.energy_threshold)])

                except sr.UnknownValueError:
                    logging.info("Sorry, not understand")
                except sr.RequestError as e:
                    logging.info("No connection")
                except KeyboardInterrupt:
                    logging.info("KeyboardInterrupt within listen loop")
                    pass
            time.sleep(1.0)

        h.close()
    except NameError as err:
        logging.info("Name error: {0}".format(err))
    except RuntimeError as err:
        logging.info("Runtime error: {0}".format(err))
    except TypeError as err:
        logging.info("Type error: {0}".format(err))
    except AttributeError as err:
        logging.info("Attribute error: {0}".format(err))
    except ValueError as err:
        logging.info("Value error: {0}".format(err))
    except KeyError as err:
        logging.info("Key error: {0}".format(err))
    except AssertionError as err:
        logging.info("Assertion error: {0}".format(err))
    except KeyboardInterrupt:
        logging.info("KeyboardInterrupt higher level")
        pass
    except:
        logging.info("Unexpected error: %s" % sys.exc_info()[0])
        pass

#===============================================================
login_params=''

logging.info('*** 初始化完成 ***')

# 为了调试时使用 Ctrl+C终止程序，
# 又能在结束时调用mspUninstall，使用了try
# 应该有更好的方法
try:

    p1 = pyaudio.PyAudio()
    devcount = p1.get_device_count()

    logging.info("Here are the available audio devices:")

    default_device_index = 0

    for device_index in range(devcount):
        device = p1.get_device_info_by_index(device_index)
        logging.info("[%s]  %s\tDefault Sample Rate: %i\t%s" % (device_index, device['name'], device['defaultSampleRate'], "Default" if device_index == default_device_index else ""))

    runMain()

    '''
    msg = u'\u809a\u75db\u6211\u597d'
    #msg = u'\u6211\u597d\u809a\u75db'
    #msg = u'\u6211\u597d\u809a\u75db\u5440'
    regexp = u'^(\u809a\u75db|(.+?)\u809a\u75db(.+?)|\u809a\u75db(.+?)|(.+?)\u809a\u75db)$'
    #regexp = u'^((.+?)\u809a\u75db(.+?))$'
    #regexp = u'^((.+?)\u809a\u75db)$'
    #regexp = u'^(\u809a\u75db(.+?))$'
    # Non-atomic triggers always need the regexp.
    match = re.match(regexp, msg)
    if match:
        print "match"
        # Collect the stars.
        stars = match.groups("")
        print len(stars)
        for s in stars:
            print " "+s
        print "here"
        stars = stars[1:]
        clean = filter(None, stars)        
        for s in clean:
            print " "+s
    '''

except Exception as e:
    logging.info(e)
finally:
    #global p
    #p.terminate()
    logging.info("reach finally")
#    mspUninstall()
    logging.info('\n*** finish ***')

# end
