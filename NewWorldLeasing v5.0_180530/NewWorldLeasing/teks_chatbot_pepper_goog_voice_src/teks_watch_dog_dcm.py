#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import os
import time

import math

from naoqi import ALProxy
from naoqi import ALBroker
import qi

from teks_my_thread import teks_thread
################################################################################

class teks_watch_dog_dcm_thread(teks_thread):

    def __init__(self, stop_event, rlock, app, cb, target, low_limit, high_limit):
        teks_thread.__init__(self, stop_event, rlock)
        
        self.app = app

        self.thread_stop = False
        self.suspend_flag = False
        self.result = ""

        self.cb = cb
        self.target = target
        self.ll = low_limit
        self.hl = high_limit

    def set_callback(self,cb):
        with self.thread_rlock:
            self.cb = cb

    def set_low_limit(self,low_limit):
        with self.thread_rlock:
            self.ll = low_limit

    def set_high_limit(self,high_limit):
        with self.thread_rlock:
            self.hl = high_limit

    def suspend(self):
        with self.thread_rlock:
            self.suspend_flag = True
        
    def suspend_beh(self):
        with self.thread_rlock:
            self.suspend_flag = True
        
    def resume(self):
        with self.thread_rlock:
            self.suspend_flag = False
            self.result = ""
            
    def resume_beh(self):
        with self.thread_rlock:
            self.suspend_flag = False
            self.result = ""
            
    def is_suspend(self):
        return (self.suspend_flag)

    def run(self):

        while not self.thread_stop_event.is_set():
            if self.suspend_flag:
                time.sleep(1.0)
                continue

            time.sleep(1.0)

            result = self.app.getData(self.target)
            #print(str(result))

            if not (self.ll <= result and result <= self.hl):
                self.cb(result)

################################################################################

import threading

if __name__ == "__main__":
  ip = "127.0.0.1"
  port = 9559
  myBroker = ALBroker("myBroker",
      "0.0.0.0",    # listen to anyone
      0,            # find a free port and use it
      ip,          # parent broker IP
      port)        # parent broker port

  memProxy = ALProxy("ALMemory", ip, port)
  prefMgrProxy = ALProxy("ALPreferenceManager", ip, port)

  session = qi.Session()
  try:
      session.connect("tcp://" + ip + ":" + str(port))
  except RuntimeError:
      logging.info("Can't connect to Naoqi at ip \"" + ip + "\" on port " + str(port) +".\nPlease check your script arguments. Run with -h option for help.")
      sys.exit(1)

  pill2kill = threading.Event()
  rlock = threading.RLock()

  def process_angle(angle):
      #jsonMessage = json.loads(message)

      #print(str(message))
      print("process angle: "+str(angle))
      pass

  watch_dog_thread_dcm = teks_watch_dog_dcm_thread(pill2kill, rlock, memProxy, process_angle, "Device/SubDeviceList/HeadYaw/Position/Actuator/Value", -math.pi/2, math.pi/2)
  watch_dog_thread_dcm.start()
  watch_dog_thread_dcm.set_name("I/am/watch/dog/dcm/headyaw")
  print watch_dog_thread_dcm.get_name()

  try:
    while True:
      try:
        watch_dog_thread_dcm.resume()
        while not watch_dog_thread_dcm.is_suspend(): time.sleep(0.01)
        watch_dog_thread_dcm.suspend()

        time.sleep(1.0)

      except Exception as e:
          print e
          pill2kill.set()
          pass

  except Exception as e:
      print e

  finally:
      print '\n*** WAIT FOR CHILD THREADS EXIT ***\n'

      pill2kill.set()
      watch_dog_thread_dcm.join()

      print '\n*** FINISH ***\n'
