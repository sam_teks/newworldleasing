
#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from py2neo import Graph, authenticate

import teks_db_configure as CONFIGURE

##################################################################

def authorize_hkia():
  '''
  graph_url = "http://158.69.217.1:7474" #should read from OS environ
  username = "neo4j"  #should read from OS environ
  password = "dv44EkoY"  #should read from OS environ
  '''
  [graph_url, username, password] = CONFIGURE.NEO4J_AUTHORIZE_PARAM_HKIA

  graph = None
  try:
    if username and password:
      authenticate(graph_url.strip("http://"), username, password)
    graph = Graph(graph_url + "/db/data")

  except Exception as e:
    print e
    graph = None

  return graph

def authorize_local():
  '''
  graph_url = "http://127.0.0.1:7474" #should read from OS environ
  username = "neo4j"  #should read from OS environ
  password = "nao"  #should read from OS environ
  '''
  [graph_url, username, password] = CONFIGURE.NEO4J_AUTHORIZE_PARAM_LOCAL

  graph = None
  try:
    if username and password:
      authenticate(graph_url.strip("http://"), username, password)
    graph = Graph(graph_url + "/db/data")

  except Exception as e:
    print e
    return None

  return graph
