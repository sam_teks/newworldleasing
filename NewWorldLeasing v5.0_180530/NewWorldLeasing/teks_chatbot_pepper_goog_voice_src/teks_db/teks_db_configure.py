#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
CONFIGURE_FILE_ABSOLUTE_PATH = os.path.split(os.path.realpath(__file__))[0]

LANGUAGE_EN = "en_us"
LANGUAGE_CN = "zh_cn"
LANGUAGE_CT = "zh_hk"

SUPPORT_LANGUAGE_EN = True
SUPPORT_LANGUAGE_CN = True
SUPPORT_LANGUAGE_CT = True

# Set neo4j authorize parameter
NEO4J_AUTHORIZE_PARAM_DENSEN =   ["http://192.168.0.155:7474", "neo4j", "nao"]
NEO4J_AUTHORIZE_PARAM_LOCAL = ["http://127.0.0.1:7474", "neo4j", "nao"]
NEO4J_AUTHORIZE_PARAM_HKIA =  ["http://158.69.217.1:7474", "neo4j", "dv44EkoY"]

UI_DB_FILE = "ui_counters.db"
UI_DB_TABLE = "ui_counters"

FUNCTION_DB_FILE = "func_log.db"

