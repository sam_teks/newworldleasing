
import threading
import logging

class MultiRecognition(object):
    def __init__(self):
        self.result = {'confidence': 0, 'transcript': ''}
        self.mylock = threading.RLock()

    def multiThreading(self, recogFunc, frames_list):
        threads = []
        for i in range(frames_list):
            threads.append(threading.Thread(target=self.recognition, args=(recogFunc, i+1)))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        self.result_set = self.result
        self.result = {'confidence': 0, 'transcript': ''}

        return self.result_set

    def recognition(self, recogFunc, idx):

        said_text = ""
        confidence = 0
        try:
            said_text, confidence = recogFunc(idx)
        except Exception as e:
            #self.result = {'confidence': 0, 'transcript': ''}
            pass

        self.mylock.acquire()
        if self.result['confidence'] < confidence:
            self.result['confidence'] = confidence
            self.result['transcript'] = said_text
        self.mylock.release()



