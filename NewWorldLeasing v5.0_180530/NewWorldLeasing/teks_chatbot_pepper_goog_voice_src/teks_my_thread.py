#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import time
import threading

class teks_thread(threading.Thread):
    def __init__(self, stop_event, rlock):
        threading.Thread.__init__(self)

        self.thread_stop_event = stop_event
        self.thread_rlock = rlock

        self.thread_stop = False
        self.suspend_flag = True

    def suspend(self):
        with self.thread_rlock:
            self.suspend_flag = True

    def resume(self):
        with self.thread_rlock:
            self.suspend_flag = False

    def is_suspend(self):
        return self.suspend_flag

    def get_id(self):
        return self.ident

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.setName(name)

################################################################################
