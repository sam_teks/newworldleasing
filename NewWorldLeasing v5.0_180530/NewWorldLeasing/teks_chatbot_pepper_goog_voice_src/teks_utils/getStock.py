# -*- coding: utf-8 -*-
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import urllib
import parse
import json
import datetime
import re
import math

def getStockInfo(sc):
    apikey = "9fb4c5f5910d82ce7a15a45f419661ca"

    url = "http://web.juhe.cn:8080/finance/stock/hk?num=" + str(sc) + "&key=" + str(apikey)

    raw = urllib.urlopen(url)
    raw = str(raw.read())

    return raw

def checkData(sc):
    try:
        val = int(sc)
    except ValueError:
        print("That's not an int!")
        jsondata = '{"Errorcode":1}'
    else:
        stockCode = str(sc)
        numofword = len(sc)
        if (numofword != 5):
            if (numofword == 1):
                stockCode = "0000" + str(sc)
            if (numofword == 2):
                stockCode = "000" + str(sc)
            if (numofword == 3):
                stockCode = "00" + str(sc)
            if (numofword == 4):
                stockCode = "0" + str(sc)
        
        raw = getStockInfo(stockCode)

        if 'data' in raw:
            item_dict = json.loads(raw)

            ename = item_dict['result'][0]['data']['ename']
            name = item_dict['result'][0]['data']['name']
            date = item_dict['result'][0]['data']['date']
            time = item_dict['result'][0]['data']['time']
            lastestpri = float(item_dict['result'][0]['data']['lastestpri'])
            uppic = float(item_dict['result'][0]['data']['uppic'])
            limit = round(float(item_dict['result'][0]['data']['limit']),2)

            print ename
            print name
            print stockCode
            print date
            print time
            print lastestpri
            print uppic
            print limit

            if (uppic == 0):
                cmg = "无起跌"
            elif (uppic > 0):
                cmg = "升：" + str(uppic) + " 元。"
            else:
                cmg = "跌：" + str(abs(uppic)) + " 元。"

            print name + "(" + stockCode + ")" + "现价:" + str(lastestpri) + "。" + cmg + "(" + str(limit) + "%)"
            jsondata = '{"Errorcode":0,"name":"' + str(name) + '","stockCode":"' + str(stockCode) + '","date":"' + str(date) + '","time":"' + str(time) + '","lastestpri":' + str(lastestpri) + ',"uppic":' + str(uppic) + ',"limit":' + str(limit) + '}'
        else:
            print "股票號碼錯誤"
            jsondata = '{"Errorcode":1}'

        return jsondata


def main():
    stockCode = raw_input(u'請輸入股票號碼: >>> ').encode('utf-8')
    cd = checkData(stockCode)
    print cd

if __name__ == "__main__":
    main()

