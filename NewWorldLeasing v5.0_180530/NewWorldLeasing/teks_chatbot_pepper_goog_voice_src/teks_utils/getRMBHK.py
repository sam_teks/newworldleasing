#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import requests
import urllib
import json
import datetime
import re

def getRMBRate():
	url = "http://www.chinalife.com.hk/customerservice/rmb-exchange"
	raw = urllib.urlopen(url)
	raw = str(raw.read())

	startpos = raw.find('<h1>人民幣兌換率</h1>')
	endpos = raw.find('<div class="footer-contact"')
	raw = raw[startpos:endpos]

	raw = re.sub('/[ ]{2,}|[\t]/', ' ',raw)
	raw = re.sub('/[ ]{1,}|[\t]/', ' ',raw)
	raw = raw.replace(' ', '')
	raw = raw.replace('  ', '')
	raw = raw.replace('<br/>', '')
	raw = raw.replace('\n', '')
	raw = raw.replace('\r', '')
	raw = raw.replace('\r\n', '')
	raw = raw.replace('&nbsp;', '')
	raw = re.sub('/\r|\n/', '', raw)
	raw = re.sub('[\s+]', '', raw)

	#rmbdate_data
	rmbdate_startpos = raw.find('<divclass="stockNoticeclearfix"><div>') + 37
	rmbdate_endpos = raw.find('</div><div>每100元人民幣兌港元牌價')
	rmbdate_data = raw[rmbdate_startpos:rmbdate_endpos]

	#print rmbdate_data

	#sellingPrice
	sellrmb_startpos = raw.find('<divclass="stockLabel">賣出價</div><divclass="stockPrice">') + 61
	sellrmb_endpos = raw.find('</div></div><divclass="left"><divclass="stockLabel">')
	sellrmb = raw[sellrmb_startpos:sellrmb_endpos]

	#print sellrmb

	#buyingPrice
	buyingrmb_startpos = raw.find('<divclass="stockLabel">買入價</div><divclass="stockPrice">') + 61
	buyingrmb_endpos = raw.find('</div><divclass="remarks">')
	buyingrmb = raw[buyingrmb_startpos:buyingrmb_endpos]

	#print buyingrmb

	#updateDate
	updatedate_startpos = raw.find('</div><divclass="remarks">') + 26
	updatedate_endpos = raw.find('</div></div></div><div></div></div></div></div></div></div>')
	updatedate = raw[updatedate_startpos:updatedate_endpos]

	#print updatedate

	m1 = "每100元人民币兑港元的牌价: 卖出价: " + sellrmb + "。买入价: " + buyingrmb + "。"
	m2 = updatedate

	print m1
	print m2

	jsondata = '{"Errorcode":0,"sellrmb":' + str(sellrmb) + ',"buyrmb":' + str(buyingrmb) + ',"updatedate":"' + str(updatedate) + '"}'

	return jsondata

def main():
	cd = getRMBRate()
	print cd

if __name__ == "__main__":
	main()