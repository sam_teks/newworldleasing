#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import os
import subprocess
import sys
import time
import json
import simplejson
import requests
import urllib2
import urllib
import threading
import teks_configure as CONFIGURE
from teks_utility import *
#from teks_asr_ifly import teks_record_thread
from teks_asr_goog_beta import teks_record_thread
from teks_tts import teks_tts_thread
from teks_led_state import led_state
from teks_audio_player import audio_player

from teks_speech_moves import teks_speech_moves_thread

from teks_omba_utils import omba_processor
import teks_brain_process as brain_process
from teks_watch_dog import teks_watch_dog_thread
from teks_watch_dog_dcm import teks_watch_dog_dcm_thread

import math

import logging
import teks_logger as teks_log

DEFAULT_LOGGER_FORMATTER = logging.Formatter('%(message)s,%(levelname)s,"%(asctime)s"')
DEFAULT_LOGGER_LEVEL = logging.INFO

logger = teks_log.teks_logger("teks_demo", \
                     DEFAULT_LOGGER_FORMATTER, \
                     None, \
                     DEFAULT_LOGGER_LEVEL)

from naoqi import ALProxy
from naoqi import ALModule
from naoqi import ALBroker
import qi

from os.path import isfile, join, splitext

################################################################################

app_starter = None
event_starter = None
loc_name = None

UNMATCHED_SENTENCE_LIST = []
CURRENT_LANGUAGE = CONFIGURE.LANG_CT
#CURRENT_LANGUAGE = CONFIGURE.LANG_CN

pill2kill = threading.Event()
rlock = threading.RLock()
last_recognized_face = None
last_recognized_face_match_level = 0
ttsProxy = None
record_thread = None

class ListeningControlModule(ALModule):
    detect_in = False

    def onLoad(self):
        global pill2kill
        global rlock

        self.faceStopped = True
        #self.vc_thread = teks_video_capture.teks_video_capture_thread(pill2kill, rlock)
        pass

    def checkFace(self):
        global pill2kill
        global rlock

        self.vc_thread = teks_video_capture.teks_video_capture_thread(pill2kill, rlock)
        self.vc_thread.start()

    def setLock(self,rlock):
        self.rlock = rlock
        pass

    def SaySeen(self,name,value):
        global memProxy
        global CURRENT_LANGUAGE
        global last_recognized_face
        global last_recognized_face_match_level

        last_recognized_face = str(memProxy.getData("Teks/lastRecognizedFace"))
        last_recognized_face_match_level = memProxy.getData("Teks/lastRecognizedFaceMatchLevel")

        print(CURRENT_LANGUAGE)
        p = "hi I see you "+str(memProxy.getData("Teks/lastRecognizedFace"))
        print(p)
        pass

    def SayMaybeSeen(self,name,value):
        global memProxy
        global CURRENT_LANGUAGE
        global last_recognized_face
        global last_recognized_face_match_level

        last_recognized_face = str(memProxy.getData("Teks/lastRecognizedFace"))
        last_recognized_face_match_level = memProxy.getData("Teks/lastRecognizedFaceMatchLevel")

        print(CURRENT_LANGUAGE)
        p = "hi mabye I see "+str(memProxy.getData("Teks/lastRecognizedFace"))+" around"
        print(p)
        pass

    def foundFace(self,name,value):
        global event_starter
        global last_recognized_face
        global last_recognized_face_match_level
        global tts_thread
        global CURRENT_LANGUAGE

        with self.rlock:
            logging.info("foundFace called: ")
            if value: logging.info("foundFace value called: "+value)
            if value:
              jsonData = json.loads(value)
              last_recognized_face_match_level = jsonData["confidence"]
              if jsonData["confidence"] >= CONFIGURE.FACE_THRESHOLD:
                  print("match: "+jsonData["transcript"])
                  matchFile = str(jsonData["transcript"])
                  last_recognized_face = splitext(matchFile)[0]
                  #say_text(tts_thread, "I think I see you, "+splitext(matchFile)[0], CONFIGURE.LANG_EN)
                  #event_starter.start("Teks/ResumeFaceDetect","1")
              elif jsonData["confidence"] >= CONFIGURE.FACE_THRESHOLD_2:
                  print("MAYBE match: "+jsonData["transcript"])
                  matchFile = str(jsonData["transcript"])
                  last_recognized_face = splitext(matchFile)[0]
                  #say_text(tts_thread, "Maybe I see, "+splitext(matchFile)[0]+" around", CONFIGURE.LANG_EN)
                  #event_starter.start("Teks/ResumeFaceDetect","1")
              else:
                  print("no match")
                  with self.rlock:
                      if not self.faceStopped:
                          self.checkFace()
                      else:
                          event_starter.start("Teks/ResumeFaceDetect","1")

        pass

    def notFoundFace(self,name,value):
        global event_starter
        global last_recognized_face

        logging.info("notFoundFace called: ")

        with self.rlock:
            if not self.faceStopped:
                last_recognized_face = None
                self.checkFace()
            else:
                event_starter.start("Teks/ResumeFaceDetect","1")
        pass

    def startFace(self,name,value):
        global last_recognized_face
        logging.info("startFace called: ")

        with self.rlock:
            self.faceStopped = False
            last_recognized_face = None
            self.checkFace()
        pass

    def stopFace(self,name,value):
        global last_recognized_face

        logging.info("stopFace called: ")
        with self.rlock:
            last_recognized_face = None
            self.faceStopped = True
        pass

    def stopProcessText(self,name,value):
        global record_thread
        
        logging.info("stopProcessText called: ")
        with self.rlock:
            if record_thread:
                logging.info("stopProcessText called suspend: ")
                record_thread.suspend_beh()
                event_starter.start("Teks/PriceRite/html_press_reject","1")
        pass

    def startProcessText(self,name,value):
        global record_thread

        logging.info("startProcessText called: ")
        with self.rlock:
            if record_thread:
                logging.info("stopProcessText called resume: ")
                record_thread.resume_beh()
                event_starter.start("Teks/PriceRite/html_press_allow","1")
        pass

    def processText(self,name,value):
        global omba_proc_ct
	    #global tts_thread
        #global reply_text
        #global event_starter
  
        logging.info("ProcessText called: "+value)
        with self.rlock:
            self.stopProcessText(None,None)
            #time.sleep(1)
            reply = omba_proc_ct.process_text(value)
            reply_text = reply.encode("utf8").strip()
            
            beh_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "behName")
            if beh_name and beh_name != "undefined":
                #event_starter.start("Teks/StartFaceDetect","1")
                loc_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "targetloc")
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                logger.info('behName,'+beh_name)
                logging.info("behName: "+beh_name)
                logger.info('targetloc:'+loc_name)
                logging.info("targetloc: "+loc_name)
                if reply_text:
                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                    say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                logger.info('reply_text,'+reply_text)
                time.sleep(0.5)
                app_starter.start(str(beh_name))
            else:
                evt_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtName")
                if evt_name and evt_name != "undefined":
                    #event_starter.start("Teks/StartFaceDetect","1")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                    evt_param = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtParam")
                    logger.info('evtName,'+evt_name+','+evt_param)
                    logging.info("evtName: "+evt_name)
                    logging.info("evtParam: "+evt_param)
                    if evt_name == "Teks/StartFaceDetect":
                        if face_recog_once:
                            evt_name = "Teks/ResumeFaceDetect"
                        face_recog_once = True
                    event_starter.start(evt_name,str(evt_param))
                    logger.info('reply_text,'+reply_text)
                    if reply_text: 
                        led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                        say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                else:
                    #if reply_text:
                    say_text(tts_thread, reply_text, "Cantonese")
                    #time.sleep(1)
            
            self.startProcessText(None,None)
        
    def detectInProcess(self,name,value):
        self.detect_in = True
        self.startProcessText(None,None)
        print ">>>>> detect In <<<<<"
        
    def detectOutProcess(self,name,value):
        self.detect_in = False
        self.stopProcessText(None,None)
        print ">>>>> detect Out <<<<<"
        
    def qr_start(self,name,value):
        self.stopProcessText(None,None)
        print ">>1>>> processText - qr_start"
        os.system("export LD_LIBRARY_PATH=/home/nao/.local/lib:$LD_LIBRARY_PATH; export PYTHONPATH=/home/nao/.local/lib/python2.7/site-packages:$PYTHONPATH; python /home/nao/.local/share/PackageManager/apps/pricerite_pepper/script/main.py")

    def qr_quit(self,name,value):
        print "qr_quit"
        #self.logger.verbose('Attempting to start tablet webview')
        #tablet = self.s.ALTabletService
        #tablet.resetTablet()
        #if tablet:
        #robot_ip = tablet.robotIp()
        
        if self.detect_in:
            print "-> qr_timeout, show_tablet_original, index1.html"
            self.startProcessText(None,None)
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
            event_starter.start("Teks/PriceRite/html_index1","1")
        else:
            print "-> qr_timeout, show_tablet_original, index.html"
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
            event_starter.start("Teks/PriceRite/html_index","1")
        #tablet.showWebview(app_url)
        #else:
        #    self.logger.warning('Lost tablet service, cannot load ' +
        #                        'application: {}'.format(self.PKG_ID))
    
    def qr_succ(self,name,value):
        print "qr_succ"
        #self.logger.verbose('Attempting to start tablet webview')
        #tablet = self.s.ALTabletService
        #tablet.resetTablet()
        #if tablet:
        #robot_ip = tablet.robotIp()
        
        #say_text(tts_thread, "親愛的實惠會員，歡迎蒞臨實惠旺角旗艦店，你已成功領取會員優惠，感謝閣下對實惠的踴躍支持！", "Cantonese")
        say_text(tts_thread, "親愛的會員，你已成功領取會員優惠，感謝閣下對實惠的踴躍支持！", "Cantonese")
        
        if self.detect_in:
            print "-> qr_succ, show_tablet_succ, index1.html"
            self.startProcessText(None,None)
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
            event_starter.start("Teks/PriceRite/html_index1","1")
        else:
            print "-> qr_succ, show_tablet_succ, index.html"
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
            event_starter.start("Teks/PriceRite/html_index","1")
        #tablet.showWebview(app_url)
        
        #else:
        #    self.logger.warning('Lost tablet service, cannot load ' +
        #                        'application: {}'.format(self.PKG_ID))
        
    def qr_fail(self,name,value):
        print "qr_fail"
        #self.logger.verbose('Attempting to start tablet webview')
        #tablet = self.s.ALTabletService
        #tablet.resetTablet()
        #if tablet:
        #robot_ip = tablet.robotIp()
        
        say_text(tts_thread,"此會員"+"帳號的優惠已被領取" , "Cantonese")
        
        if self.detect_in:
            print "-> qr_fail, show_tablet_fail, index1.html"
            self.startProcessText(None,None)
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
            event_starter.start("Teks/PriceRite/html_index1","1")
        else:
            print "-> qr_fail, show_tablet_fail, index.html"
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
            event_starter.start("Teks/PriceRite/html_index","1")
        #tablet.showWebview(app_url)
        
        #else:
        #    self.logger.warning('Lost tablet service, cannot load ' +
        #                        'application: {}'.format(self.PKG_ID))

    def qr_notmember(self,name,value):
        print "qr_notmember"
        #self.logger.verbose('Attempting to start tablet webview')
        #tablet = self.s.ALTabletService
        #tablet.resetTablet()
        #if tablet:
        #robot_ip = tablet.robotIp()
        
        say_text(tts_thread,"系統未能找到此會員帳號" , "Cantonese")
        
        if self.detect_in:
            print "-> qr_notmember, show_tablet_notmember, index1.html"
            self.startProcessText(None,None)
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
            event_starter.start("Teks/PriceRite/html_index1","1")
        else:
            print "-> qr_notmember, show_tablet_notmember, index.html"
            #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
            event_starter.start("Teks/PriceRite/html_index","1")
        #tablet.showWebview(app_url)
        
        #else:
        #    self.logger.warning('Lost tablet service, cannot load ' +
        #                        'application: {}'.format(self.PKG_ID))


    def process_angle(self, angle):
        print("ANGLE: over the limits: "+str(angle))
        self.logger.info("ANGLE: over the limits: "+str(angle))
        pass
    
    def dispatchBehavior(self):
        global app_starter
        global loc_name

        if loc_name:
          logging.info("dispatchBehavior: "+loc_name)
          if loc_name == "frontdoor":
              app_starter.start("teks_pepperuno/goto_front_door")
              say_text(tts_thread, "here you go", CONFIGURE.LANG_EN)
          elif loc_name == "gotostanley":
              app_starter.start("teks_pepperuno/goto_stanley")
              say_text(tts_thread, "here you go", CONFIGURE.LANG_EN)

          loc_name = None

        pass

def foulwords_concept(text):
    foulWords = ["tmd","piss off", "bullshit", "drop dead", "asshole", "bitch", "son of a bitch", "son of bitch", "shit", "fuck", "fucking", "cun", "your ass", "贱人", "屌", "屌你", "老母", "屌老母系", "扑街", "食屎", "屎忽", "去死", "操你", "操你妈", "傻逼", "冚家铲", "冚家祥","頂***","*你老母","僕你個街","仆*","你個*","*你個*","廢柴","*你個臭*","臭*","含家產","食屎","撚樣"]
    return match_concept(text, foulWords)
    
################################################################################
ip = "127.0.0.1"
port = 9559
myBroker = ALBroker("myBroker",
    "0.0.0.0",    # listen to anyone
    0,            # find a free port and use it
    ip,          # parent broker IP
    port)        # parent broker port

memProxy = ALProxy("ALMemory", ip, port)
prefMgrProxy = ALProxy("ALPreferenceManager", ip, port)
motionProxy = ALProxy("ALMotion", ip, port)
managerProxy = ALProxy("ALBehaviorManager", ip, port)

session = qi.Session()
try:
    session.connect("tcp://" + ip + ":" + str(port))
except RuntimeError:
    logging.info("Can't connect to Naoqi at ip \"" + ip + "\" on port " + str(port) +".\nPlease check your script arguments. Run with -h option for help.")
    sys.exit(1)

listenController = ListeningControlModule("listenController")
listenController.setLock(rlock)
listenController.onLoad()
    
if __name__ == '__main__':
  from teks_db import teks_db_api
  
  db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
  db_log.clear_counter("ui_counters","counter")
  db_log.close()

  write_pid_file("pid.log")

  randomMoveController = RandomMoveControlModule("randomMoveController")

  motionProxy.wakeUp()
  
  #memProxy.subscribeToEvent("Teks/SaySeen","listenController", "SaySeen")
  #memProxy.subscribeToEvent("Teks/SayMaybeSeen","listenController", "SayMaybeSeen")
  
  memProxy.subscribeToEvent("Teks/FoundFace","listenController", "foundFace")
  memProxy.subscribeToEvent("Teks/NotFoundFace","listenController", "notFoundFace")
  memProxy.subscribeToEvent("Teks/FaceDetected","listenController", "startFace")
  memProxy.subscribeToEvent("Teks/NotFaceDetected","listenController", "stopFace")

  #memProxy.subscribeToEvent("Teks/StopProcessText","listenController", "stopProcessText")
  #memProxy.subscribeToEvent("Teks/StartProcessText","listenController", "startProcessText")
  
  
  
  memProxy.subscribeToEvent("Teks/PriceRite/DetectOut","listenController", "detectOutProcess")
  memProxy.subscribeToEvent("Teks/PriceRite/DetectIn","listenController", "detectInProcess")
  
  memProxy.subscribeToEvent("Teks/PriceRite/html","listenController", "processText")
  #memProxy.subscribeToEvent("Teks/PriceRite/html_member","listenController", "processText")
  memProxy.subscribeToEvent("Teks/PriceRite/html_qrscan","listenController", "qr_start")
  
  memProxy.subscribeToEvent("Teks/PriceRite/qr_quit","listenController", "qr_quit")
  memProxy.subscribeToEvent("Teks/PriceRite/qr_succ","listenController", "qr_succ")
  memProxy.subscribeToEvent("Teks/PriceRite/qr_fail","listenController", "qr_fail")
  memProxy.subscribeToEvent("Teks/PriceRite/qr_notmember","listenController", "qr_notmember")
  
  import os

  import pyaudio
  p1 = pyaudio.PyAudio()
  devcount = p1.get_device_count()

  logging.info("Here are the available audio devices:")

  default_device_index = 0

  for device_index in range(devcount):
      device = p1.get_device_info_by_index(device_index)
      logging.info("[%s]  %s\tDefault Sample Rate: %i\t%s" % (device_index, device['name'], device['defaultSampleRate'], "Default" if device_index == default_device_index else ""))


  tts_thread = teks_tts_thread(pill2kill, rlock)
  record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",7)
  moves_thread = teks_speech_moves_thread(pill2kill, rlock)
  #record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",1)
  #record_thread.record_device_index = 7
  #record_thread.set_input(CURRENT_LANGUAGE)

  watch_dog_thread = teks_watch_dog_thread(pill2kill, rlock)
  watch_dog_headyaw_thread = teks_watch_dog_dcm_thread(pill2kill, rlock, memProxy, listenController.process_angle, "Device/SubDeviceList/HeadYaw/Position/Actuator/Value", -math.pi/2, math.pi/2)

  omba_proc_ct = omba_processor("./pepper/ct/brain_pr")
  omba_proc_cn = None
  omba_proc_en = None
  #omba_proc_cn = omba_processor("./pepper/cn/brain")
  #omba_proc_en = omba_processor("./pepper/en/brain")
  #omba_proc_ct = omba_processor("./nao/ct/brain")
  #omba_proc_cn = omba_processor("./nao/cn/brain")

  tts_thread.start()
  record_thread.start()
  moves_thread.start()

  watch_dog_thread.start()
  watch_dog_thread.set_name("I/am/watch/dog")

  watch_dog_headyaw_thread.start()
  watch_dog_headyaw_thread.set_name("I/am/watch/dog/headyaw")

  led = led_state()
  aplayer = audio_player()

  app_starter_chatbot = app_starter_module("app_starter_chatbot")
  app_starter_chatbot.start_aysn("pricerite_pepper/main")

  #app_starter_facerecog = app_starter_module("app_starter_facerecog")
  #app_starter_facerecog.start_aysn("teks_face_recog/main")

  app_starter = app_starter_module("app_starter")
  event_starter = event_starter_module("event_starter")

  #event_starter.start("Teks/StartFaceDetect","1")
  face_recog_once = False

  curPath = os.path.dirname(os.path.abspath(__file__))    

  uid = CONFIGURE.REQUEST_UID
  url = CONFIGURE.REQUEST_URL

  LAST_SPEECH_LANGUAGE = CONFIGURE.LANG_CT
  prefMgrProxy.setValue("robot.config.userlang", "spokenlanguage", LAST_SPEECH_LANGUAGE)

  #CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT, CONFIGURE.GOOG_LANG_CN, CONFIGURE.GOOG_LANG_EN]
  #CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT, CONFIGURE.GOOG_LANG_CN:CONFIGURE.LANG_CN, CONFIGURE.GOOG_LANG_EN:CONFIGURE.LANG_EN}

  #CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT, CONFIGURE.GOOG_LANG_CN]
  #CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT, CONFIGURE.GOOG_LANG_CN:CONFIGURE.LANG_CN}

  #CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT, CONFIGURE.GOOG_LANG_EN]
  #CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT, CONFIGURE.GOOG_LANG_EN:CONFIGURE.LANG_EN}

  CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT]
  CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT}

  print '\n*** INITIALIZE SUCCESS ***\n'

  import random
  '''
  wait_text_list = [
                        "",
                        "請等等",
                        "",
                        "請等一等",
                        "",
                        "唔該等等",
                        "",
                        "唔該等一等",
                        "",
                        "好"]
  '''
  wait_text_list = [
                        "",
                        "",
                        "唔",
                        "",
                        "",
                        "好"]


  try:
    #record_thread.resume()
    suspend_listening_flag = False
    while True:
      try:
        #check to proceed or not
        if suspend_listening_flag:
            time.sleep(0.1)
            continue

        #print "CURRENT_LANGUAGE:", CURRENT_LANGUAGE
        # STT process
        '''
        with rlock:
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_ASR_PROCESS)
          aplayer.play(curPath+"/begin_reco.wav")
        '''
        start_time = time.time()
        input_text, speech_lang = listen_text(record_thread, CURRENT_LANGUAGE)

        '''
        wait_text = random.choice(wait_text_list)
        if wait_text and input_text:
          say_text(tts_thread, wait_text, CURRENT_LANGUAGE)
        '''
        '''
        with rlock:
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_PROCESS_PROCESS)
          aplayer.play(curPath+"/end_reco.wav")
        '''
        '''
        lang_setting = check_current_language(input_text)
        if lang_setting:
          CURRENT_LANGUAGE = set_current_language(tts_thread, lang_setting, CURRENT_LANGUAGE)
          continue
        '''

        if input_text:
          logging.info("=====================THANK YOU========"+str(time.time()-start_time)+"======")
          input_text = c1 = input_text.encode("utf8")
          logging.info( "I heard 1: "+c1)

          db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
          db_log.inc_counter_by_key("ui_counters",input_text,"counter")
          db_log.close()

          #msg = raw_input('You> combined_row,'
          msg = c1.lower()

          logger.info('input_text,'+input_text+' '+speech_lang)
          if ("永远再见" == input_text) or ("永再見" == input_text) or ("永遠再見" == input_text) or ("forever bye bye" == input_text.lower()):
            event_starter.start("TEKS/StandUpPose","1")
            break

          #if speech_lang != CONFIGURE.LANG_CT:
          #  CURRENT_LANGUAGE = CONFIGURE.LANG_CT

          #  logging.info("not really Cantonese")
          #  reply_text = "我讀得書少 淨係識廣東話 你唔好恰我"
          #  moves_thread.resume()
          #  say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
          #  moves_thread.suspend()

          #  LAST_SPEECH_LANGUAGE = CURRENT_LANGUAGE
            #reset back to use Cantonese ASR to listen English and Cantonese
          #  with rlock:
          #    time.sleep(1.0)
          #    suspend_listening_flag = False
          #  continue


          # check speaking language
          reply = ""
          cur_omba = None
		  #CURRENT_LANGUAGE = CONFIGURE.LANG_CT
		  
          cur_omba = omba_proc_ct
          if speech_lang == CONFIGURE.LANG_CT:
            CURRENT_LANGUAGE = check_language(c1,CURRENT_LANGUAGE)
            #cur_omba = omba_proc_ct
            print "check user speaking "+speech_lang
          elif speech_lang == CONFIGURE.LANG_EN:
            CURRENT_LANGUAGE = check_language(c1,CURRENT_LANGUAGE)
            #cur_omba = omba_proc_en
            print "check user speaking "+speech_lang
          else:
            CURRENT_LANGUAGE = CONFIGURE.LANG_CN
            #cur_omba = omba_proc_cn
            print "check user speaking "+speech_lang

          CURRENT_LANGUAGE = CONFIGURE.LANG_CT
          print "current pepper speaking "+CURRENT_LANGUAGE
          prefMgrProxy.setValue("robot.config.userlang", "spokenlanguage", CURRENT_LANGUAGE)

          chk_msg = msg
          if foulwords_concept(msg):
              logging.info("=====================Detected Foul Language========"+msg+"======")
              chk_msg = "四零四"
          start_time = time.time()
          
          reply = cur_omba.process_text(chk_msg)
          
          logging.info(time.time()-start_time)

          #
          logging.info("=====================My Reply========"+reply.encode("utf8")+"======")
          reply_text = reply.encode("utf8").strip()

          #if reply_text: event_starter.start("Dialog/LastInput","1")
          if cur_omba:
              listenController.stopProcessText(None,None)
                
              event_starter.start("Teks/Dialog/LastInput","1")
              page_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "pageName")
              beh_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "behName")
              qrcode_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "qrCode")
              
              if qrcode_name and qrcode_name != "undefined":
                  self.stopProcessText(None,None)
                  print ">>>>> while_loop - qr_start"
                  os.system("export LD_LIBRARY_PATH=/home/nao/.local/lib:$LD_LIBRARY_PATH; export PYTHONPATH=/home/nao/.local/lib/python2.7/site-packages:$PYTHONPATH; python /home/nao/.local/share/PackageManager/apps/pricerite_pepper/script/main.py")
                  cur_omba.bot.set_uservar(CONFIGURE.REQUEST_UID, "qrCode","")
              elif beh_name and beh_name != "undefined":
                  #event_starter.start("Teks/StartFaceDetect","1")
                  loc_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "targetloc")
                  cur_omba.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                  logger.info('behName,'+beh_name)
                  logging.info("behName: "+beh_name)
                  logger.info('targetloc:'+loc_name)
                  logging.info("targetloc: "+loc_name)
                  if reply_text:
                      led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                      say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                  logger.info('reply_text,'+reply_text)
                  time.sleep(0.5)
                  app_starter.start(str(beh_name))
                  #event_starter.start("TEKS/SetStartPt",str(evt_param))
              else:
                evt_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtName")
                if evt_name and evt_name != "undefined":
                    #event_starter.start("Teks/StartFaceDetect","1")
                    cur_omba.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                    evt_param = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtParam")
                    logger.info('evtName,'+evt_name+','+evt_param)
                    logging.info("evtName: "+evt_name)
                    logging.info("evtParam: "+evt_param)
                    if evt_name == "Teks/StartFaceDetect":
                        if face_recog_once:
                            evt_name = "Teks/ResumeFaceDetect"
                        face_recog_once = True
                    event_starter.start(evt_name,str(evt_param))
                    logger.info('reply_text,'+reply_text)
                    if reply_text: 
                        led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                        say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                else:
                    #event_starter.start("TEKS/FINISH_SHOW_SCREEN","1")
                    if not reply_text:
                        #reply_text = make_reply_natural(reply_text, CURRENT_LANGUAGE)
                        CURRENT_LANGUAGE = LAST_SPEECH_LANGUAGE

                    logger.info('reply_text,'+reply_text)

                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)

                    if reply_text:
                      #event_starter.start("Teks/StartFaceDetect","1")
                      #randomMoveController.runBR()
                      speech_lang = check_language(reply_text,CURRENT_LANGUAGE)
                      if CURRENT_LANGUAGE == CONFIGURE.LANG_CT:
                        moves_thread.resume()
                        say_text(tts_thread, reply_text, speech_lang)
                        moves_thread.suspend()
                      #elif speech_lang == CONFIGURE.LANG_EN:
                      #  say_text(tts_thread, reply_text, speech_lang)
                      #else:
                      #  say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                    #randomMoveController.stand(3.0)
                    
              listenController.startProcessText(None,None)

              last_recognized_face = None

        if watch_dog_thread.is_modified():
            #reset flag here
            omba_proc_ct = omba_processor("./pepper/ct/brain_pr")
            #omba_proc_cn = omba_processor("./pepper/cn/brain")
            #omba_proc_en = omba_processor("./pepper/en/brain")

            reply = omba_proc_ct.process_text("一零一")

            watch_dog_thread.reset_modified_flag()

        LAST_SPEECH_LANGUAGE = CURRENT_LANGUAGE

        #reset back to use Cantonese ASR to listen English and Cantonese
        CURRENT_LANGUAGE = CONFIGURE.LANG_CT
        with rlock:
          #time.sleep(1.0)
          suspend_listening_flag = False

        '''
        input_json = {"inputText": input_text, "uid": uid};

        start_time = time.time()
        req = requests.post(url, json = input_json)
        if req:
          print "post time: " + str(time.time() - start_time)

          print '\n*** REQUEST RETURNS ***\n'
          req_content = str(req.content)
          print "req_content: " + req_content

          reply_text = get_reply_from_json(req_content)
          print "reply_text: " + reply_text

          reply_text = make_reply_natural(reply_text, CURRENT_LANGUAGE)
          print "我 > " + reply_text
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
          say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
        '''

      except Exception as e:
        print e
        pill2kill.set()
        pass

  except Exception as e:
    print e

  finally:
    print '\n*** WAIT FOR CHILD THREADS EXIT ***\n'

    #save_unmatched_sentence(UNMATCHED_SENTENCE_LIST)  # If rivescript can match nothing, save the text to file.
    app_starter_chatbot.stop("pricerite_pepper/main")
    #app_starter_facerecog.stop("teks_face_recog/main")

    pill2kill.set()

    tts_thread.join()
    record_thread.join()
    moves_thread.join()
    watch_dog_thread.join()
    watch_dog_headyaw_thread.join()

    led.eyes_reset()

    print '\n*** STANDING UP ***\n'
    event_starter.start("Teks/ManuallyEndPythonApp","1")
    randomMoveController.stand(3.0)

    print '\n*** FINISH ***\n'
