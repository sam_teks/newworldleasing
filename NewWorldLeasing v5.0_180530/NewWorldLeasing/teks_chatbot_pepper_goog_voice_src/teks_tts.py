#!/usr/bin/python
# -*- coding: utf-8 -*-

############################################################

import sys
import time
import ctypes

from naoqi import ALProxy
from naoqi import ALBroker
import qi

import teks_configure as CONFIGURE

import teks_speech_process as sp_process

################################################################################

#ttsso = ctypes.CDLL(CONFIGURE.IFLY_TTS_LINK_LIB_PATH)
ttsso = None

################################################################################

# HELP FUNCTION
def get_iflyteks_tts_params(lang = CONFIGURE.LANG_CN):
    tts_params = ""
    if lang == CONFIGURE.LANG_CN:
        tts_params = CONFIGURE.IFLY_TTS_LANG_PARAM_CHINESE
    elif lang == CONFIGURE.LANG_CT:
        tts_params = CONFIGURE.IFLY_TTS_LANG_PARAM_CANTONESE
    elif lang == CONFIGURE.LANG_EN:
        tts_params = CONFIGURE.IFLY_TTS_LANG_PARAM_ENGLISH
    else:
        tts_params = CONFIGURE.IFLY_TTS_LANG_PARAM_CHINESE  # Default for Putonghua
    return tts_params

################################################################################

################################################################################

class tts(object):
    def __init__(self):

        # NOTICE: Iflyteks Account Can only Login ONCE!!! If ONLY use TTS, You Can uncomment this block.
        '''
        if  ttsso.ttsLogin(CONFIGURE.IFLY_LOGIN_PARAM) != 0 :    # SUCCESS CODE is 0
            ttsso.ttsLogOut()
            sys.exit()
        '''

        self.tts_proxy = ALProxy("ALTextToSpeech", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        self.animated_speech_proxy = ALProxy("ALAnimatedSpeech", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        self.audio_player_proxy = ALProxy("ALAudioPlayer", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        self.memory_proxy = ALProxy("ALMemory", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        self.ct_tts_proxy = ALProxy("cantonese_tts_service", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        #self.ct_tts_proxy = None

        self.ttsing_flag = False
        self.lang = CONFIGURE.LANG_CN

    def __def__(self):
        #ttsso.ttsLogOut()
        pass

    def nao_tts(self, text = "", lang = CONFIGURE.LANG_CN):
        try:
            start_time = time.time()

            if self.lang != lang:
                self.lang = lang

            if lang == CONFIGURE.LANG_CT:
                self.ttsing_flag = True
                #self.memory_proxy.raiseEvent("TEKS/SayAnimCantonese",text)
                #self.ct_tts_proxy.say_text(text)
                sp_process.speech_loop(text,CONFIGURE.LANG_CT, self.tts_proxy, self.animated_speech_proxy, self.ct_tts_proxy)
                print "TTS Time (Cantonese): " + str(time.time() - start_time)
                self.ttsing_flag = False
                return

            if lang == CONFIGURE.LANG_CN:
                #self.ct_tts_proxy.say_text("","",text)
                self.tts_proxy.setLanguage("Chinese")
                self.animated_speech_proxy.say(text)
                print "TTS Time (Putonghua): " + str(time.time() - start_time)
                self.ttsing_flag = False
                return

            if lang == CONFIGURE.LANG_EN:
                #self.ct_tts_proxy.say_text("",text,"")
                self.tts_proxy.setLanguage("English")
                self.animated_speech_proxy.say(text)
                print "TTS Time (English): " + str(time.time() - start_time)
                self.ttsing_flag = False
                return

            '''
            self.tts_proxy.setLanguage(lang)
            self.ttsing_flag = True
            #self.animated_speech_proxy.say(text, {"bodyLanguageMode": "random"})
            self.tts_proxy.say(text)
            self.ttsing_flag = False
            '''
            print "TTS Time: " + str(time.time() - start_time)

        except Exception as e:
            print e

    def ifly_tts(self, text = "", lang = CONFIGURE.LANG_CN):
        try:
            tts_params = get_iflyteks_tts_params(lang)

            '''
            # NOTICE: Iflyteks Account Can only Login ONCE!!! If ONLY use TTS, You Can uncomment this block.
            if  ttsso.ttsLogin(CONFIGURE.IFLY_LOGIN_PARAM) == 0 :    # SUCCESS CODE is 0
                ttsso.text_to_speech(text, CONFIGURE.NAO_TTS_OUTPUT_WAVE_PATH, tts_params)
            ttsso.ttsLogOut()
            '''

            ttsso.text_to_speech(text, CONFIGURE.NAO_TTS_OUTPUT_WAVE_PATH, tts_params)

            file_id = self.audio_player_proxy.loadFile(CONFIGURE.NAO_TTS_OUTPUT_WAVE_PATH)  #file Path
            self.audio_player_proxy.play(file_id)
            self.audio_player_proxy.stop(file_id)
            self.audio_player_proxy.unloadFile(file_id)
        except Exception as e:
            print e

################################################################################

from teks_my_thread import teks_thread

class teks_tts_thread(teks_thread):
    def __init__(self, stop_event, rlock):
        teks_thread.__init__(self, stop_event, rlock)

        self.text = ""
        self.result = []
        self.tts = tts()
        self.lang = CONFIGURE.LANG_CN

    def set_input(self, text = "", lang = CONFIGURE.LANG_CN):
        self.text = text
        self.lang = lang

    def get_output(self):
        return self.result

    def run(self):
        while not self.thread_stop_event.is_set():
            if self.suspend_flag:
                time.sleep(0.1)
                continue

            # do something
            self.tts.nao_tts(self.text, self.lang)
            while self.tts.ttsing_flag:
                time.sleep(0.1)

            self.suspend_flag = True

################################################################################

import threading

if __name__ == "__main__":

    pill2kill = threading.Event()
    rlock = threading.RLock()

    tts_thread = teks_tts_thread(pill2kill, rlock)
    tts_thread.start()

    test_data_set = [["你好！我是机器人！", CONFIGURE.LANG_CN], ["Hello, I am Robot!", CONFIGURE.LANG_EN], ["你好，我係機械人！", CONFIGURE.LANG_CT]]

    for test_data in test_data_set:
        tts_thread.set_input(test_data[0], test_data[1])
        tts_thread.resume()
        while not tts_thread.is_suspend(): time.sleep(0.01)
        tts_thread.suspend()

    pill2kill.set()
    tts_thread.join()
