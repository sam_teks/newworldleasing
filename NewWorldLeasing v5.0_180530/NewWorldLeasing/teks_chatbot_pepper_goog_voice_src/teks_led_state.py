#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from naoqi import ALProxy
import teks_configure as CONFIGURE

class led_state(object):
    def __init__(self, robot_ip = CONFIGURE.NAO_ALPROXY_IP, robot_port = CONFIGURE.NAO_ALPROXY_PORT):
        self.leds = ALProxy("ALLeds", robot_ip, robot_port)
        self.audio_player_proxy = ALProxy("ALAudioPlayer", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)

    def eyes_fade(self, color, duration = 0.1):
        self.leds.off('FaceLeds')
        color_value = 256 * 256 * color[0] + 256 * color[1] + color[2]
        id = self.leds.post.fadeRGB("FaceLeds", color_value, duration)
        self.leds.wait(id, 0)

    def eyes_intensity(self, value):
        self.leds.setIntensity("FaceLeds", value)

    def eyes_stop(self):
        self.leds.off('FaceLeds')

    def eyes_reset(self):
        self.leds.off('FaceLeds')
        self.leds.reset("FaceLeds")

    def eyes_rotate(self, duration = 0.1):
        self.leds.off('FaceLeds')
        #rgb = 0x3366FF
        rgb = 0x0000FF
        id = self.leds.post.rotateEyes(rgb, 1.0, duration)
        self.leds.wait(id, 0)

if __name__ == "__main__":

    led = led_state()
    led.eyes_stop()

    led.eyes_fade([0, 255, 0])
    time.sleep(5.0)
    led.eyes_rotate(5.0)

    led.eyes_reset()
