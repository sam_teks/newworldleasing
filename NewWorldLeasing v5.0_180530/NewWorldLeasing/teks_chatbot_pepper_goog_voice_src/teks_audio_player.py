#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from naoqi import ALProxy
import teks_configure as CONFIGURE

class audio_player(object):
    def __init__(self, robot_ip = CONFIGURE.NAO_ALPROXY_IP, robot_port = CONFIGURE.NAO_ALPROXY_PORT):
        self.audio_player_proxy = ALProxy("ALAudioPlayer", robot_ip, robot_port)
        self.fileId = None

    def play(self, filename):
        self.fileId = self.audio_player_proxy.loadFile(filename)  #file Path
        self.audio_player_proxy.play(self.fileId)
        self.audio_player_proxy.stop(self.fileId)
        self.audio_player_proxy.unloadFile(self.fileId)
        self.fileId = None        

    def stop(self):
        if self.fileId:
            self.audio_player_proxy.stop(self.fileId)
        pass

if __name__ == "__main__":
    import os

    aplayer = audio_player()
    curPath = os.path.dirname(os.path.abspath(__file__))    
    aplayer.play(curPath+"/begin_reco.wav")
