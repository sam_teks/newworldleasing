#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import sys
import time
import json
import requests
import threading

from teks_asr_goog_beta import teks_record_thread

import logging
DEFAULT_LOGGER_FORMATTER = logging.Formatter('%(message)s,%(levelname)s,"%(asctime)s"')
DEFAULT_LOGGER_LEVEL = logging.INFO

################################################################################

if __name__ == '__main__':
  import os

  import pyaudio
  p1 = pyaudio.PyAudio()
  devcount = p1.get_device_count()

  logging.info("Here are the available audio devices:")

  default_device_index = 0

  for device_index in range(devcount):
      device = p1.get_device_info_by_index(device_index)
      logging.info("[%s]  %s\tDefault Sample Rate: %i\t%s" % (device_index, device['name'], device['defaultSampleRate'], "Default" if device_index == default_device_index else ""))


  print '\n*** FINISH ***\n'
