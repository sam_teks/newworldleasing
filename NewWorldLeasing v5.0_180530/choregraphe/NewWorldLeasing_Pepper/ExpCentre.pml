<?xml version="1.0" encoding="UTF-8" ?>
<Package name="ExpCentre" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="main" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="pose_handshake" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="pose_hug" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="pose_high5" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="hungdance" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="Intllvdance" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="datedance" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="test" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="photo_time_ct" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="photo_time_en" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="say" src="say/say.dlg" />
        <Dialog name="ExampleDialog" src="main/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="bootstrap.min" src="html/css/bootstrap.min.css" />
        <File name="index" src="html/index.html" />
        <File name="awesomplete" src="html/js/awesomplete.js" />
        <File name="awesomplete.min" src="html/js/awesomplete.min.js" />
        <File name="bootstrap.min" src="html/js/bootstrap.min.js" />
        <File name="jquery.mobile.custom.min" src="html/js/jquery.mobile.custom.min.js" />
        <File name="main" src="html/js/main.js" />
        <File name="robotutils.1.0" src="html/js/robotutils.1.0.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
        <File name="videoutils" src="html/js/videoutils.js" />
        <File name="click" src="html/click.wav" />
        <File name="videoutils.min" src="html/js/videoutils.min.js" />
        <File name="bootstrap" src="html/js/bootstrap.js" />
        <File name="js.cookie" src="html/js/js.cookie.js" />
        <File name="robotutils.min" src="html/js/robotutils.min.js" />
        <File name="01_applause" src="html/01_applause.ogg" />
        <File name="main" src="main.png" />
        <File name="font-awesome.min" src="html/css/font-awesome.min.css" />
        <File name="DINRoundOffcPro-Black" src="html/fonts/DINRoundOffcPro-Black.ttf" />
        <File name="DINRoundOffcPro-Bold" src="html/fonts/DINRoundOffcPro-Bold.ttf" />
        <File name="DINRoundOffcPro-Light" src="html/fonts/DINRoundOffcPro-Light.ttf" />
        <File name="DINRoundOffcPro-Medium" src="html/fonts/DINRoundOffcPro-Medium.ttf" />
        <File name="DINRoundOffcPro" src="html/fonts/DINRoundOffcPro.ttf" />
        <File name="FontAwesome" src="html/fonts/FontAwesome.otf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.eot" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.svg" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.ttf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff2" />
        <File name="" src=".DS_Store" />
        <File name="" src="html/.DS_Store" />
        <File name="README" src="script/butane/README" />
        <File name="__init__" src="script/butane/__init__.py" />
        <File name="conversation" src="script/butane/conversation.py" />
        <File name="fuel" src="script/butane/fuel.py" />
        <File name="language_utils" src="script/butane/language_utils.py" />
        <File name="package_utils" src="script/butane/package_utils.py" />
        <File name="main" src="script/main.py" />
        <File name="pr_promo_code" src="script/pr_promo_code.py" />
        <File name="__init__" src="script/stk/__init__.py" />
        <File name="__init__" src="script/stk/__init__.pyc" />
        <File name="events" src="script/stk/events.py" />
        <File name="events" src="script/stk/events.pyc" />
        <File name="logging" src="script/stk/logging.py" />
        <File name="logging" src="script/stk/logging.pyc" />
        <File name="runner" src="script/stk/runner.py" />
        <File name="runner" src="script/stk/runner.pyc" />
        <File name="services" src="script/stk/services.py" />
        <File name="services" src="script/stk/services.pyc" />
        <File name="teks_configure" src="script/teks_configure.py" />
        <File name="click" src="html/click.ogg" />
        <File name="face" src="html/img/face.png" />
        <File name="face1" src="html/img/face1.png" />
        <File name="main_old" src="script/main_old.py" />
        <File name="01_Hungary" src="hungdance/01_Hungary.mp3" />
        <File name="02_Hungary" src="hungdance/02_Hungary.ogg" />
        <File name="InternationalLove" src="Intllvdance/InternationalLove.mp3" />
        <File name="music_all" src="datedance/music_all.mp3" />
        <File name="main_camera" src="html/js/main_camera.js" />
        <File name="angular-PapaParse" src="html/js/angular-PapaParse.js" />
        <File name="angular-PapaParse.min" src="html/js/angular-PapaParse.min.js" />
        <File name="angular-fastclick.min" src="html/js/angular-fastclick.min.js" />
        <File name="angular-route.min" src="html/js/angular-route.min.js" />
        <File name="angular-touch" src="html/js/angular-touch.js" />
        <File name="angular-touch.min" src="html/js/angular-touch.min.js" />
        <File name="angular.min" src="html/js/angular.min.js" />
        <File name="papaparse.min" src="html/js/papaparse.min.js" />
        <File name="teksphotolib" src="script/teksphotolib.py" />
        <File name="moment.min" src="html/js/moment.min.js" />
        <File name="plyr" src="html/css/plyr.css" />
        <File name="plyr" src="html/js/plyr.js" />
        <File name="FutuMd" src="html/app/css/FutuMd.ttf" />
        <File name="MSblack" src="html/app/css/MSblack.ttf" />
        <File name="btnconfig" src="html/app/css/btnconfig.css" />
        <File name="common" src="html/app/css/common.css" />
        <File name="style" src="html/app/css/style.css" />
        <File name="style_plus" src="html/app/css/style_plus.css" />
        <File name="action" src="html/app/js/action.js" />
        <File name="b1" src="html/img/main/b1.jpg" />
        <File name="b1b1" src="html/img/main/b1b1.jpg" />
        <File name="b1b1b1" src="html/img/main/b1b1b1.jpg" />
        <File name="b1b1b3b2" src="html/img/main/b1b1b3b2.jpg" />
        <File name="b1b1b3b3" src="html/img/main/b1b1b3b3.jpg" />
        <File name="jquery-3.1.1.min" src="html/js/jquery-3.1.1.min.js" />
        <File name="robotutils_2.0" src="html/js/robotutils_2.0.js" />
        <File name="p1" src="html/img/main/p1.png" />
        <File name="recording" src="html/img/recording.gif" />
        <File name="b1b1b2" src="html/img/main/b1b1b2.jpg" />
        <File name="b1b1b3b1" src="html/img/main/b1b1b3b1.jpg" />
        <File name="transparent" src="html/img/main/transparent.png" />
        <File name="menu_option1" src="html/img/menu_option1.png" />
        <File name="menu_option2" src="html/img/menu_option2.png" />
        <File name="FSCloseBtn" src="html/img/FSCloseBtn.png" />
        <File name="imgConfig" src="html/img/imgConfig.js" />
        <File name="menu_option2_old" src="html/img/menu_option2_old.png" />
        <File name="miscconfig" src="html/app/css/miscconfig.css" />
        <File name="Slide1" src="html/img/main/Slide1.jpg" />
        <File name="Slide11" src="html/img/main/Slide11.jpg" />
        <File name="Slide12" src="html/img/main/Slide12.jpg" />
        <File name="Slide13" src="html/img/main/Slide13.jpg" />
        <File name="b1b1b3" src="html/img/main/b1b1b3.jpg" />
        <File name="b1b1b4" src="html/img/main/b1b1b4.jpg" />
        <File name="b1b2" src="html/img/main/b1b2.jpg" />
        <File name="b1b2b1" src="html/img/main/b1b2b1.jpg" />
        <File name="b1b2b2" src="html/img/main/b1b2b2.jpg" />
        <File name="b1b2b3" src="html/img/main/b1b2b3.jpg" />
        <File name="b1b2b3b1" src="html/img/main/b1b2b3b1.jpg" />
        <File name="b1b2b3b2" src="html/img/main/b1b2b3b2.jpg" />
        <File name="b1b2b3b3" src="html/img/main/b1b2b3b3.jpg" />
        <File name="b1b2b4" src="html/img/main/b1b2b4.jpg" />
        <File name="btnconfig_111" src="html/app/css/btnconfig_111.css" />
        <File name="index" src="html/index.js" />
        <File name="menu_option3" src="html/img/menu_option3.png" />
        <File name="menu_option4" src="html/img/menu_option4.png" />
    </Resources>
    <Topics>
        <Topic name="say_enu" src="say/say_enu.top" topicName="say" language="en_US" />
        <Topic name="lexicon_enu" src="say/lexicon_enu.top" topicName="lexicon" language="en_US" />
        <Topic name="ExampleDialog_enu" src="main/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="Lexicon_enu" src="script/butane/dialog/Lexicon/Lexicon_enu.top" topicName="Lexicon" language="en_US" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
