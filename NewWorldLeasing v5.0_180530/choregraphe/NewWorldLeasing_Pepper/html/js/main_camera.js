var PriceRiteStore = angular.module('PriceRiteStore', ['ngTouch','ngRoute', 'ngPapaParse']);
var catalog = null;
var sections = null;
var users = Object();

var goBackAllowed = true;

// Parse Product Catalog
Papa.parse(
  'catalog/best_buy.csv',
  {
    header: true,
    dynamicTyping: true,
    complete: function(data){
      catalog = data.data;
      data.data.forEach(function(product){
        product.alternate = JSON.parse(product.alternate); 
      });
      RobotUtils.onService(function(ALMemory) {
        ALMemory.raiseEvent('PriceRiteStore/Catalog', catalog);
      });
      console.log('Product catalog loaded');
    },
    download: true
  });

// Parse Store Map Sections
Papa.parse(
  'sections/best_buy.csv',
  {
    header: true,
    dynamicTyping: true,
    complete: function(data){
      var parsed = {};
      data.data.forEach(function(section){
        var name = section.section_name.toLowerCase().split(" ").join("_");
        var region = JSON.parse(section.region);
        var keywords = JSON.parse(section.keywords);
        var pin_location = JSON.parse(section.pin_location);
        parsed[name] = [region, keywords, pin_location];
      });
      sections = parsed;
      RobotUtils.onService(function(ALMemory) {
        ALMemory.raiseEvent('PriceRiteStore/Keywords', parsed);
      });
      console.log('Store map sections loaded');
    },
    download: true
  });

// Parse Users Database
Papa.parse(
  'users/best_buy.csv',
  {
    header: true,
    dynamicTyping: true,
    complete: function(data){
      data.data.forEach(function(user){
        try {
          user.wishlist = JSON.parse(user.wishlist);
        } catch(e) {
          console.log(e);
          user.wishlist = [];
        }
        users[user.id] = user;

      });
      console.log('User DB loaded');
    },
    download: true
  });

PriceRiteStore.config(["$locationProvider", "$routeProvider", function($locationProvider, $routeProvider) {

  /*
  .when('/', {
    templateUrl : 'pages/main.html',
    controller  : 'mainController'
  })
*/

  $routeProvider  
  .when('/', {
    templateUrl : 'pages/login.html',
    controller  : 'loginController'
  })
  .when('/loyalty/:id/getpoints', {
    templateUrl : 'pages/loyalty_points.html',
    controller  : 'loyaltyPointsController'
  })
  /*
  .when('/map/:section/:block?', {
    templateUrl : 'pages/map.html',
    controller  : 'mapController'
  })
  
  .when('/search/:type/:query?', {
    templateUrl : 'pages/search.html',
    controller  : 'searchController'
  })

  .when('/product/:sku', {
    templateUrl : 'pages/product.html',
    controller  : 'productController'
  })

  .when('/scan/:sku?', {
    templateUrl : 'pages/scan.html',
    controller  : 'scanController'
  })

  .when('/loyalty/:id/:first_time?', {
    templateUrl : 'pages/loyalty.html',
    controller  : 'loyaltyController'
  })
  
  .when('/loyalty', {
    templateUrl : 'pages/login.html',
    controller  : 'loginController'
  })

  .when('/login', {
    templateUrl : 'pages/login.html',
    controller  : 'loginController'
  })

  .when('/signup', {
    templateUrl : 'pages/signup.html',
    controller  : 'signupController'
  })

  .when('/logo', {
    templateUrl : 'pages/logo.html',
    controller  : 'logoController'
  })
  */
  .otherwise({
    redirectTo: '/'
  });
}]);

PriceRiteStore.filter('decodeURI', function() {
  return window.decodeURIComponent;
});

PriceRiteStore.filter('encodeCategory', function() {
  var fct = function(val) {
    return val.toLowerCase().split(' ').join('_');
  };
  return fct;
});

//Directive for tablet feedbacks
var sound = new Audio("click.ogg")
PriceRiteStore.directive('playSound', function(){
  return {
    restrict:'A',
      priority: 100, // give it higher priority than built-in ng-click
      link: function(scope, element, attrs) {
        element.bind('touchstart', function(event){
          setTimeout(function(){
            sound.play();
          }, 1);
        });
      },
    };
  }) 

var main_product;
var similar;
var upsell;
var product_loc;
var scan_value;
var scan_location;
var search_by;
var search_query;
var login_scan;
var product_scan;
var scan_locked = false;

function get_product(sku) {
  var sku_prod = null;
  for (product in catalog) {
    if (catalog[product].sku == sku) {
      sku_prod = catalog[product];
      break;
    }
  }
  return sku_prod;
}

function get_product_by_name(name) {
  var name_prod = null;
  for (product in catalog) {
    if (catalog[product].product_name_sanitized == name) {
      name_prod = catalog[product];
      break;
    }
    
    catalog[product]['alternate'].forEach(function(alternate) {
      if (alternate == name) {
        name_prod = catalog[product];       
      }
    });
    if (name_prod) { break; };
  }
  return name_prod;
}

function unsubscribe_scanners() {
  try {login_scan.unsubscribe();} catch(e) {}
  try {product_scan.unsubscribe();} catch(e) {}
}

function homeChoice(choice) {
  setTimeout(function(){
    RobotUtils.onService(function(ALMemory) {
      ALMemory.raiseEvent('yield', 'audio_tour,'+ choice);
    });
  },1);
};

PriceRiteStore.controller('productController', function($scope, $rootScope, $routeParams, $window, $timeout) {

  $scope.similar = null;
  $scope.upsell = null;
  $scope.product = get_product($routeParams.sku);
  $scope.showModal = false;
  $scope.isConnected = true;
  $scope.state = "init"

  var twilio = null;

  function refreshBuyOnline_button(isConnected){
    $scope.isConnected = isConnected;
    $scope.$apply();
  }

  RobotUtils.onService(function(Twilio) {
    twilio = Twilio
    twilio.is_connected().then(function(isConnected){
      refreshBuyOnline_button(isConnected)
    })
  }, function(){
      refreshBuyOnline_button(false);
  });

  $scope.$on("$destroy", function(){
    try {buyonline_evt.unsubscribe();} catch(e) {}
  });

  var buyonline_evt = RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/BuyOnline', function(value){
    $scope.start_buy_online();
    $scope.$apply(); // refresh the view
  })

  $scope.checkForDeal = function(sku){
    if($rootScope.user){
      return ($routeParams.sku == $rootScope.user.deal);
    } else {
      return ($routeParams.sku == catalog[0].sku);
    }
  };

  $scope.start_buy_online = function(){
    homeChoice('buy_online')
    $scope.showModal = true;
  }

  $scope.sendLink = function(){

    $scope.state = "sending";
    homeChoice('sending_link')
    var to_number = document.getElementsByName("phone_number")[0].value;
    var from_number = "+19788177133"
    var msg = "Hey, it's Pepper, here is the link to buy online this product : "+main_product.product_name_display
    msg+=" https://developer.softbankrobotics.com "

    twilio.send_sms(to_number, from_number, msg, null).then(function(){
      $timeout(function(){
        $scope.state = "sent"
        homeChoice('link_sent')
        $timeout(function(){
          $scope.showModal = false;
          $scope.state = "init"
        }, 2000);
      }, 5000);
    })
  }

  if ($scope.product != null) {
    main_product = $scope.product;
    $scope.similar = get_product($scope.product.similar);
    $scope.upsell = get_product($scope.product.upsell);
    switch(main_product.stock){
      case 0:
      homeChoice('product_out_stock'); break;
      case 1:
      homeChoice('product_low_stock'); break;
      default:
      homeChoice('product_in_stock');
    }
    if($scope.checkForDeal()) homeChoice('daily_deal');
  } else {
    $window.location.assign('#/');
  }
});

PriceRiteStore.controller('mapController', function($scope, $rootScope, $routeParams) {

  $scope.grid = $.map(Array(91), function(val, i){ return i+1; });
  $scope.selectedSection = 'sec_' + $routeParams.section;
  $scope.selectedBlock = $routeParams.block;

  console.log("$routeParams : ", $routeParams)
  if($routeParams.section == "none"){
    homeChoice('map');
  }else{
    if ($scope.selectedBlock != "nospeak"){
      RobotUtils.onService(function(ALMemory) {
        ALMemory.raiseEvent('yield', 'audio_tour,'+ $routeParams.section);
      });
    }
  } 

  $scope.highlightSection = function(){
    if($scope.selectedSection != 'sec_none') {
      try {
        document.getElementById($scope.selectedSection).setAttribute('class', 'map_section map_section_selected');
      } catch (e) {
        console.log('Cannot highlight that section.');
      }
    }
  };
});

PriceRiteStore.controller('loyaltyPointsController', function($scope, $rootScope, $routeParams, $window) {
	//alert('login_user_getpoints,'+ $routeParams.id);
	$member_id = $routeParams.id;
  RobotUtils.onService(function(ALMemory) {
	//$abc='login_user_getpoints,'+ $member_id;
	//alert($member_id);
    ALMemory.raiseEvent('yield', 'login_user_getpoints,'+ $member_id);
  });
  console.log("$routeParams check1: ", $routeParams.id)
  $rootScope.user = users[$routeParams.id];
  $scope.wishlist = [];
  $scope.deal = null;

  if($rootScope.user) {
    RobotUtils.onService(function(ALMemory) {
      ALMemory.insertData('PriceRiteStore/User', $rootScope.user.first_name);
    });
  } else {
    $window.location.assign('#/');
  }    
});

PriceRiteStore.controller('loyaltyController', function($scope, $rootScope, $routeParams, $window) {

  $rootScope.user = users[$routeParams.id];
  $scope.wishlist = [];
  $scope.deal = null;

  if($rootScope.user) {
    RobotUtils.onService(function(ALMemory) {
      ALMemory.insertData('PriceRiteStore/User', $rootScope.user.first_name);
    });
    $rootScope.user.wishlist.forEach(function(sku) {
      $scope.wishlist.push(get_product(parseInt(sku)));
    });
    $scope.deal = get_product($rootScope.user.deal);
    if($routeParams.first_time){
      RobotUtils.onService(function(ALMemory) {
        ALMemory.raiseEvent('yield', 'audio_tour,loyalty');
      });
    }
  } else {
    $window.location.assign('#/');
  }

  $scope.getFormattedPhone = function(){
    return (!!$rootScope.user.phone)? "-" : $rootScope.user.phone.slice(0,-4)+"****";
  }

  $scope.getFormattedEmail = function(){
    return ($rootScope.user.email)?  "-" : $rootScope.user.email.split('@')[0]+"@******.com";
  }
});

PriceRiteStore.controller('searchController', function($scope, $routeParams){

  homeChoice('search');

  $scope.catalog = catalog;
  $scope.search = Object();
  $scope.searchBy = $routeParams.type;
  if ($scope.searchBy == 'name') {
    $scope.searchBy = 'product_name_display';
  }
  $scope.query = $routeParams.query;
  if($scope.query) {
    $scope.search[$scope.searchBy] = $routeParams.query;
  }


  $scope.keyPress = function(event) {
    if (event.which == 13) {
      $('#query').blur();
    }
  };

  $scope.searchKeyPress = function(btn) {
    $('#query, #search_btn').blur();
  };

  $scope.clearSearch = function() {
    $('#query').val('');
    $scope.search[$scope.searchBy] = '';
  };
});

PriceRiteStore.controller('scanController', function($scope, $routeParams, $window){

  $scope.ready = false;

  $scope.$on("$destroy", function(){
    unsubscribe_scanners();
    try {waiting_evt.unsubscribe();} catch(e) {}
  });

  $scope.scan = null;
  if ($routeParams.sku) {
    $scope.scan = get_product($routeParams.sku);
    start_video();
  }else{
    homeChoice('scan');
  }

  scan_value = null;

  function start_scan(){
    product_scan = RobotUtils.subscribeToALMemoryEvent(
      'BarcodeReader/BarcodeDetected',
      function(value){
        if (!scan_locked) {
          scan_locked = true;    
          if (value.length == 1){
            value = value[0][0];
            if (value != scan_value) {
              scan_value = value;
              console.log('QR Code value: ' + scan_value);
              $window.location.assign('#/scan/' + scan_value);
              RobotUtils.onService(function(ALAudioPlayer){
                //ALAudioPlayer.playFile('/home/nao/.local/share/PackageManager/apps/mgie_pricerite_scancode/sounds/bip.ogg');
              });
            }
          }
          $window.setTimeout(function() {
            scan_locked = false;
          }, 2000);
        }
      },
      function(){ 
        console.log('BarcodeReader Event Subscribed');
      }
      );
  }

  function start_video(){
    RobotUtils.onService(function (ALVideoDevice, ALListeningMovement) {
      ALListeningMovement.setEnabled(false)
      try {
        VideoUtils.unsubscribeAllHandlers(ALVideoDevice, 'video_buffer_camera').then(function(){
          VideoUtils.startVideo(ALVideoDevice, 'video_buffer', 0, 10, 0);
          $scope.ready = true;
          $scope.$apply();
        });
      } catch(e) {
        VideoUtils.startVideo(ALVideoDevice, 'login_video_buffer', 0, 10, 0);
      }
    });
    start_scan();
  }

  var waiting_evt = RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/Scan', function(value){
    if(value != "ready") return;
    start_video();
  })
});

PriceRiteStore.controller('loginController', function($scope, $rootScope, $routeParams, $window, $location){

  homeChoice('loyalty');

  console.log($rootScope.user);

  if(!!$rootScope.user) window.location.replace('#/loyalty/' + $rootScope.user.id);

  $scope.ready = false;

  $scope.$on("$destroy", function(){
    unsubscribe_scanners();
    try {waiting_evt.unsubscribe();} catch(e) {}
  });

  function start_scan(){
    login_scan = RobotUtils.subscribeToALMemoryEvent(
      'BarcodeReader/BarcodeDetected',
      function(value){
        RobotUtils.onService(function (ALListeningMovement) {
          ALListeningMovement.setEnabled(true)
        });
        if(!scan_locked) {
          scan_locked = true;
          if (value.length == 1){
            value = value[0][0];
            if (value != scan_value) {
              console.log('QR Code value: ' + value);
              RobotUtils.onService(function(ALAudioPlayer){
                //ALAudioPlayer.playFile('/home/nao/.local/share/PackageManager/apps/mgie_pricerite_scancode/sounds/bip.ogg');
              });
			  //alert(value);
              window.location.replace('#/loyalty/' + value + '/getpoints');
            }
          }
          $window.setTimeout(function() {
            scan_locked = false;
          }, 2000);
        }
      },
      function(){ console.log('BarcodeReader Event Subscribed'); }
      );
  }

  var waiting_evt = RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/Loyalty', function(value){
    if(value != "ready") return;
    RobotUtils.onService(function (ALVideoDevice, ALListeningMovement) {
      ALListeningMovement.setEnabled(false)
      try {
        VideoUtils.unsubscribeAllHandlers(ALVideoDevice, 'video_buffer_camera').then(function(){
          VideoUtils.startVideo(ALVideoDevice, 'video_buffer', 0, 10, 0);
          $scope.ready = true;
          $scope.$apply();
        });
      } catch(e) {
        VideoUtils.startVideo(ALVideoDevice, 'login_video_buffer', 0, 10, 0);
      }
    });
    start_scan();
  }
  )
  
  RobotUtils.onService(function (ALVideoDevice, ALListeningMovement) {
    ALListeningMovement.setEnabled(false)
    try {
      VideoUtils.unsubscribeAllHandlers(ALVideoDevice, 'video_buffer_camera').then(function(){
        //VideoUtils.startVideo(ALVideoDevice, 'video_buffer', 0, 10, 0);
        VideoUtils.startVideo(ALVideoDevice, 'video_buffer', 0, 10, 13);
        $scope.ready = true;
        $scope.$apply();
      });
    } catch(e) {
      VideoUtils.startVideo(ALVideoDevice, 'login_video_buffer', 0, 10, 0);
      //VideoUtils.startVideo(ALVideoDevice, 'login_video_buffer', 3, 10, 0);
    }
  });
  
  start_scan();
});

PriceRiteStore.controller('signupController', function($scope, $window){

  $scope.$on("$destroy", function(){
    try {signed_evt.unsubscribe();} catch(e) {}
  });

  $scope.accountSignUp = function(name, email) {
    $('#submit').hide();
    if(!name && !email){
      $('#error_message').text('Missing name and improper or missing email');
      $('#error').show();
    } else if (!name) {
      $('#error_message').text('Missing name');
      $('#error').show();      
    } else if (!email) {
      $('#error_message').text('Improper or missing email');
      $('#error').show();
    } else {
      $('#pending').text('Signing up...');
      $('#pending').show();
      RobotUtils.onService(function(ALMemory) {
        ALMemory.raiseEvent('PriceRiteStore/Signee', name);
        ALMemory.raiseEvent('yield', 'audio_tour,'+ 'signup');
      });
    }
  };

  $('#signup_name').focus(function(){
    $('#success').hide();
    $('#pending').hide();
    $('#error').hide();
    $('#submit').show();
  });
  
  $('#signup_email').focus(function(){
    $('#success').hide();
    $('#pending').hide();
    $('#error').hide();
    $('#submit').show();
  });

  $scope.keyPress = function(event) {
    if (event.which == 13) {
      $('#signup_name').blur();
      $('#signup_email').blur();
    }
  };

  var signed_evt = RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/Signed', function(value) {
    if (value == 1) { $('#pending').hide(); $('#success').show(); }
  });  
});

// PriceRiteStore.controller('logoController', function($scope, $window){
// });

PriceRiteStore.controller('mainController', function($scope, $rootScope, $window) {
  //$rootScope.user = curr_user;
});

/* Preload content */
PriceRiteStore.run(function($rootScope, $http, $window, $location) {
  $http.get('pages/main.html');
  $http.get('pages/map.html');
  $http.get('pages/search.html');
  $http.get('pages/product.html');
  $http.get('pages/scan.html');
  $http.get('pages/loyalty.html');


  $rootScope.goBack = function() {
    $window.history.back();
  };

  $rootScope.goHome = function(){
    $window.location.href='#main';
  }

  $rootScope.audioTour = function(block) {
    for (var section in sections) {
      if ($.inArray(block, sections[section][0]) != -1) {
        //$window.location.assign('#/map/' + section);
        $location.path('/map/' + section).replace();
        break;
      }
    }
  };

  $rootScope.logout = function() {
    RobotUtils.onService(function(ALMemory) {
      ALMemory.raiseEvent('PriceRiteStore/Logout', '1');
    });
    $rootScope.user = null;
    $window.location.assign('#');
  };

  $rootScope.goto = function(destination) {
    if (destination == 'search') {
      destination = 'search/name';
    } else if (destination == 'map') {
      destination = 'map/none';
    } else if (destination == 'daily_deal') {
      if ($rootScope.user){
        destination = 'product/' + $rootScope.user.deal;
      } else {
        destination = 'product/' + catalog[0].sku;
      }
    } else if (destination == 'loyalty') {
      if($rootScope.user) {
        destination = 'loyalty/'+ $rootScope.user.id;
      }
    }
    $window.location.assign('#/' + destination  + '/');
  };

  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/goBack', function(value) {
    if (goBackAllowed && value == '1') {
      goBackAllowed = false;
      $window.setTimeout(function() {
        goBackAllowed = true;
      }, 2000);
      $rootScope.goBack();
    }
  });
  
  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/goto', function(value) {
    $rootScope.goto(value);
  });

  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/Logout', function(value) {
    if (value == '0') {$rootScope.logout();}
  });

  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/gotoProduct', function(name) {
    var prod = get_product_by_name(name);
    if (prod) {
      $window.location.assign('#/product/' + prod.sku);
    }
  });
  
  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/showOnMap', function(value) {
    var prod = get_product_by_name(value);
    if (prod) {
      //$window.location.assign('#/map/' + prod.category.toLowerCase().split(' ').join('_') + '/'+ prod.location);
      window.location.replace('#/map/' + prod.category.toLowerCase().split(' ').join('_') + '/'+ prod.location);
    }
  });

  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/highlightSection', function(value) {
    //$window.location.assign('#/map/' + value);
    window.location.replace('#/map/' + value + "/nospeak");
  });

  RobotUtils.subscribeToALMemoryEvent('PriceRiteStore/showProdBySection', function(value) {
    $window.location.assign('#/search/category/' + value.split('_').join(' '));
  });

});
