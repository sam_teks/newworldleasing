#!/usr/bin/env python
"""Teksbotics Take Photo App module
"""

from requests_toolbelt import MultipartEncoder
import requests
import os
import string
import random
from PIL import Image
import qrcode

import magic
mime = magic.Magic(mime=True)


__author__ = "Kerney Wu"
__copyright__ = "Copyright 2018"
__credits__ = ["Kerney Wu"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Kerney Wu"
__email__ = "kerney.wu@gmail.com"
__status__ = "Production"

API_URL = "http://faces.teksbotics.com/teksphoto/"
BLENDED_IMAGE_FILENAME = "/home/nao/recordings/cameras/blended_image.jpg"
QR_IMG_FILENAME = "/home/nao/recordings/cameras/qr_code_image.png"
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def main():
	photo_path = "/home/nao/recordings/cameras/image.jpg"
	frame_path = "/home/nao/recordings/cameras/Camera_Photoframe.png"
	blended_path = blend_image(photo_path, frame_path)
	print(blended_path)
	url = upload_image(BLENDED_IMAGE_FILENAME)
	print(url)
	qr_code_path = url2qrcode(url)
	print(qr_code_path)
	
def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

def blend_image(photo_path, frame_path):
	'''Overlay the frame image on the photo, return the absolution path of the output image
	'''
	photo = Image.open(photo_path)
	frame = Image.open(frame_path)
	#width, height = photo.size
	new_photo = photo.resize((675, 505), Image.ANTIALIAS)
	#new_frame = frame.resize((width, height), Image.ANTIALIAS)
	frame.paste(new_photo, (49, 49))
	frame.save(BLENDED_IMAGE_FILENAME)
	abs_path = os.path.join(BASE_DIR, BLENDED_IMAGE_FILENAME)
	return abs_path

def upload_image(image_path):
	'''Upload an image and return the URL of the image
	'''
	try:
		fields = {}
		fields['img_file'] = (id_generator()+".jpg", open(image_path, 'rb'), mime.from_file(image_path))
		data = MultipartEncoder(fields=fields)
		res = requests.post(API_URL+"upload", data=data, headers={'Content-Type': data.content_type, 'Accept':'application/json'})
		filename, file_extension = os.path.splitext(res.text)
		if file_extension == ".jpg":
			return API_URL+res.text
		else:
			return None
	except IOError:
		print("Error: Fail to open file")
		return None

def url2qrcode(url):
	'''Generate QR code for the image URL, return absolute path of the QR code image
	'''
	qr_img = qrcode.make(url)
	qr_img.save(QR_IMG_FILENAME)
	abs_path = os.path.join(BASE_DIR, QR_IMG_FILENAME)
	return abs_path


if __name__ == '__main__':    #code to execute if called from command-line
    main()
#use this either for a simple example of how to use the module,
#or when the module can meaningfully be called as a script.
